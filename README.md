# libProbaTree

## Description
libProbaTree aims at structuring spectroscopic (line-by-line) data by constructing a probability tree for efficient sampling of spectral lines according to their importance.
It also provides a majorant of the absorption coefficient.

Main assumptions:
- binary tree with one line per leaf by default, the tree is built using a bottom-up strategy
- lines are partitioned according to their central wavenumber
- the spectrum is represented by a polyline (i.e. points and a linear interpolation between those points)
- a line simplification procedure ([Ramer-Douglas-Peucker](https://en.wikipedia.org/wiki/Ramer%E2%80%93Douglas%E2%80%93Peucker_algorithm) algorithm) may be applied when merging sibling polylines to form the polyline of the parent node
- a tree might be computed for several pressures, then a linear interpolation in pressure is performed


## Requirements
- C++ compiler supporting at least C++17
- C compiler supporting at least C99
- CMake version >= 3.12
- [LBLU](https://gitlab.com/edstar/lblu)
- OpenMP >= 4.5 (optional but recommended for maximal efficiency)  
- Matplotlib and Python (optional, to run visualization scripts only)


## How to build
Building must be done outside of the source tree. 
Once built and installed, the library and tools might be used from external projects.

To build and install, from the base directory do

    mkdir build && cd build
    cmake [options] ..
    make install

where `[options]` are
- `-DCMAKE_PREFIX_PATH=/absolute/path/to/lblu/install` to specify LBLU installation directory.
- `-DCMAKE_INSTALL_PREFIX=/path/to/install_dir/` to specify installation directory.
  - Default installation directory is `basePath/install/{CMAKE_BUILD_TYPE}`
- `-DBUILD_TESTS=ON|OFF` to eventually build the validation tests, located in `unit_tests` directory, run with `make test` (default is OFF).
- `-DACTIVE_PARALLEL_POLICY=ON|OFF` to enable parallel computation when building the tree (default is OFF, this feature is buggy).


## Use libProbaTree as a library
To configure and link an application to libProbaTree, you have to do the following
- Add the dependency to your `CMakeLists.txt`

        find_package(SpectralProbabilityTree)
        ... 
        add_executable(exe_name source_files)
        target_link_libraries(exe_name PRIVATE MCGRad::SpectralProbabilityTree)

- configure and compile your project

        mkdir build && cd build
        cmake -DCMAKE_PREFIX_PATH=/path/to/install_dir/lib/cmake ..
        make

See `examples/XXX_ProgramTemplate` directories for an application template for C or C++ langages.
For a full example relying on libProbaTree, see [RadForcE](https://gitlab.com/yanissnp/RadForcE), a Monte-Carlo estimator of a mean flux integrated over the whole Earth and over a climatic period.


## Additionnal tools 

### buildProbaTree
This tool builds and saves a probability tree from a given line-by-line database file extracted from [HITRAN online](https://hitran.org/) (see below).
One tree is built per species (i.e. one file should be given as an input for each species), for specific thermodynamic conditions (pressure, temperature, concentration), and it is possible to provide several pressures.
The spectral range is defined by the input data file (considering the first and last spectral line wavenumbers' and line cutoff).

Run `buildProbaTree -h` for the full list of arguments.

#### Input file format
We use the following specific input format from HITRAN:
- Molecule ID
- Isotopologue ID
- $\nu$
- $S$
- $\gamma_{air}$
- $\gamma_{self}$
- $E"$
- $n_{air}$
- $\delta_{air}$


### printProbaTreeData
This tool prints data stored in the tree to file, either the absorption coefficient or line probabilities. 

Run `dumpProbaTree -h` for the full list of arguments. 

#### Output file format
The output file format is the following:
- if the exact or stored absorption coefficient are asked:

        nu_1    value_1
        nu_2    value_2
        ...
        nu_N    value_N

- if the probabilities are asked, or the unit transitions absorption (rather than their sum):

        #transition 0
        nu_1    value_1
        ...
        nu_N    value_N
        #transition 1
        nu_1    value_1
        ...
        nu_N    value_N


#### Visualization
Some additional scripts for tree visualization are provided in `examples/visu_scripts`, by running `printProbaTreeData` and plotting the output spectra.


### Starter pack
As a starter pack, a script that generates 4 lines, then builds a tree and visualizes it is provided. 
Two figures are generated, one to show the hierarchical absorption coefficient stored in the tree and the second the line probabilities.

    sh examples/starter_pack/run.sh build


## Licence
Copyright (c) 2021 - 2024, Université Paul Sabatier, CNRS.
This code was developed as part of the [ANR MCG-Rad project](https://anr.fr/Projet-ANR-18-CE46-0012).
Contact: megane.bati@irit.fr

libProbaTree is free software released under the GPLv3+ licence: GNU GPL version 3 or later. 
You can freely study, modify or extend it.
You are also welcome to redistribute it under certain conditions, refer to the [licence](https://www.gnu.org/licenses/gpl-3.0.html) for details (file LICENSE).

## Reference 
```
@softwareversion{barbier:hal-04554696v1,
  TITLE = {{libProbaTree}},
  AUTHOR = {Barbier, Wilhem and Bati, M{\'e}gane and Himeur, Chems-Eddine and Mellado, Nicolas and Nyffenegger-P{\'e}r{\'e}, Yaniss and Paulin, Mathias},
  URL = {https://hal.science/hal-04554696},
  NOTE = {},
  YEAR = {2024},
  MONTH = Apr,
  SWHID = {swh:1:dir:df408d4fb9951e4aa8e1a6cb311353b9079647c8;origin=https://gitlab.com/mbati/libprobatree/;visit=swh:1:snp:b5a1ee8245eb35604596e285597590de83dde6a0;anchor=swh:1:rev:e4ba59616f11ef8f07104c74235ceb22e157f7ec},
  REPOSITORY = {https://gitlab.com/mbati/libprobatree/},
  LICENSE = {GNU General Public License v3.0 or later},
  HAL_ID = {hal-04554696},
  HAL_VERSION = {v1},
}
```
