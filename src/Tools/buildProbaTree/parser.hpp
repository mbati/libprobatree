/* Copyright (C) 2021-2024 Université Paul Sabatier, CNRS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */


#pragma once
#include <string>
#include <vector>

class parser
{
public:
  struct Args { 
    std::string path;
    std::string name;
    bool create;
    std::vector<double> pressure;
    int leaf_size;
    double line_cutoff;
    double concentration;
    double temperature;
    bool voigt_profile;
    int children_per_node;
    double precision;
    bool majorant;

    Args() {}

    Args( std::string b,
          std::string n,
          bool cr,
          std::vector<double> p,
          int ls,
          double lc,
          double ccn,
          double temp,
          bool v,
          int k,
          double eps,
          bool maj ) :
        path( std::move( b ) ),
        name( std::move( n ) ),
        create( cr ),
        pressure( std::move( p ) ),
        leaf_size( ls ),
        line_cutoff( lc ),
        concentration( ccn ),
        temperature(temp),
        voigt_profile( v ),
        children_per_node( k ),
        precision( eps ),
        majorant( maj ) {}
  };

public:
  parser( int argc, char** argv );

private:
  bool argument_parser( const std::vector<std::string>& args );
  bool exit_error(std::string error_str);
  static void help();

public:
  Args args_;

private:
  std::string error_msg_ {""};
};
