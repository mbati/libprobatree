/* Copyright (C) 2021-2024 Université Paul Sabatier, CNRS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */


#include "parser.hpp"
#include "cluster.hpp"
#include "transition.h"
#include <algorithm>

#include <iostream>
#include <chrono>

int main(int argc, char **argv)
{
  std::vector<Transition> transitions;
  SpectralProbabilityTrees tree;
  load_molar_mass_file();

  const auto args = parser(argc, argv).args_;

  std::string FILE_PATH = args.name + ".sav";
  if (args.create) {
      tree.pressures = args.pressure;
      std::sort(tree.pressures.begin(), tree.pressures.end());
      tree.temperature = args.temperature;
      std::cout << "Creating a probability tree from file " << args.path << std::endl;
  } else {
      exit(1);
  /*
      std::cout << "[deprecated functionality: we rather build one tree per species] " << args.path << std::endl;
      std::cout << "Adding a probability tree from file " << args.path << std::endl;
      std::fstream in;
      in.open(FILE_PATH, std::ios::in | std::ios::binary);
      if ( in.fail() )
      {
        std::cerr << "Couldn't open file: " << FILE_PATH << std::endl;
        std::cerr << "Please make sure this tree already exist or use -n option " << std::endl;
        return 1;
      }
      tree = probatree_unserialize( in );
      // TODO: we should test whether temperatures and pressures are compatible
      // and pressure should then be optional when ! args.create
  */
  }

  std::cout << "Parameters:" << std::endl; 
  std::cout << "- Pressures : [";
  for(double p : tree.pressures) std::cout << " " << p << " ";
  std::cout << "]" << std::endl;
  std::cout << "- Temperature: " << tree.temperature << std::endl;
  std::cout << "- Concentration: " << args.concentration << std::endl;
  std::cout << "- Line cutoff: " << args.line_cutoff << std::endl;
  std::cout << "- Profile: " << (args.voigt_profile ? "Voigt" : "Lorentz") << std::endl;
  std::cout << "- Number of children per node: " << args.children_per_node << std::endl;
  std::cout << "- Leaf size: " << args.leaf_size << std::endl;
  std::cout << "- Precision: " << args.precision << std::endl;
  std::cout << "- Majorant: " << (args.majorant ? "yes" : "no") << std::endl;
  std::cout << std::endl;

  load_transitions( args.path, transitions );
  std::cout << "Encoding " << transitions.size() << " transitions in the probability tree." << std::endl;

  tree.species.push_back(
        classify_transitions( std::move( transitions ), args.children_per_node, args.leaf_size ));

  Specie& specie = tree.species.back();
  if (transitions.size() > 0)
    specie.name = get_molecule_name(transitions[0].molec_id);

  size_t num = probatree_nclusters(tree);
  std::cout << "Number of clusters per pressure value : " << num << std::endl;
  num *= tree.pressures.size();
  std::cout << "Total number of clusters        : " << num << std::endl;

  auto start = std::chrono::steady_clock::now();
  probatree_compile(
      tree,
      tree.species.size() - 1,
      [num]( int done ) { std::cout << "Done: " << done << "/" << num << "\r"; },
      // []( int done ) { },
      args.line_cutoff,
      args.concentration,
      args.voigt_profile,
      args.precision,
      args.majorant);
  auto end = std::chrono::steady_clock::now();
  std::cout << std::endl << "Tree created in "
    << std::chrono::duration<double>(end - start).count()
    << " second(s)" << std::endl;

  std::cout << "Save to file " << FILE_PATH << std::endl;
  std::fstream f;
  f.open(FILE_PATH, std::ios::out | std::ios::binary);
  probatree_serialize( tree, f );
  f.close();
  return 0;
}
