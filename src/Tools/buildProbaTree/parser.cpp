/* Copyright (C) 2021-2024 Université Paul Sabatier, CNRS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */


#include "parser.hpp"
#include <fstream>
#include <iostream>
#include <stdexcept>
#include <cstdlib>
#include "cluster.hpp"

#define DEFAULT_LINE_CUTOFF 25.0
#define DEFAULT_SIMPLIFICATION_CRITERION 0.05
#define DEFAULT_CONCENTRATION 1.0

parser::parser( int argc, char** argv ) {
  std::vector<std::string> line;
  for ( int i = 1; i < argc; ++i )
  {
    line.emplace_back( argv[i] );
  }
  if ( !argument_parser( line ) ) { 
    help();
    if (error_msg_ != "") {
      throw std::invalid_argument(error_msg_);
    }
    else {
      exit(0);
    }
  }
}

bool parser::exit_error(std::string error_str) {
  error_msg_ = error_str;
  return false;
}

bool parser::argument_parser( const std::vector<std::string>& args ) {
  std::string input_data_file;
  std::string output_tree_file;
  bool create          = true;
  // Thermodynamic conditions
  std::vector<double> pressure;
  double concentration  = DEFAULT_CONCENTRATION;
  double temperature    = DEFAULT_TEMPERATURE; // Default HITRAN value 
  double line_cutoff    = DEFAULT_LINE_CUTOFF;
  bool profile          = false;
  // Tree parameters
  int lines_per_leaf    = 1;
  int children_per_node = 2;
  bool is_majorant      = false;
  double RDP_precision  = DEFAULT_SIMPLIFICATION_CRITERION;
 
  bool pathflag     = false;
  bool nameflag     = false;
  bool pressureflag = false;

  for ( size_t i = 0; i < args.size(); ++i )
  {
    if ( args[i] == "-b" )
    {
      ++i;
      if ( i >= args.size() ) return exit_error("-b flag needs a string argument");
      pathflag = true;
      input_data_file = args[i];
    }
    else if ( args[i] == "-n" )
    {
      ++i;
      if ( i >= args.size() ) return exit_error("-n flag needs a string argument");
      nameflag = true;
      output_tree_file     = args[i];
    }
    /*
    // Adding to an existing tree is no longer handled
    else if ( args[i] == "-a" )
    {
      ++i;
      if ( i >= args.size() ) return exit_error("-a flag needs a string argument");
      nameflag = true;
      output_tree_file = args[i];
      create   = false;
    }
    */
    else if ( args[i] == "-k" )
    {
      ++i;
      if ( i >= args.size() ) return exit_error("-k flag needs an argument");
      children_per_node = std::stoi( args[i] );
    }
    else if ( args[i] == "-s" )
    {
      ++i;
      if ( i >= args.size() ) return exit_error("-s flag needs an argument");
      lines_per_leaf = std::stoi( args[i] );
      if (is_majorant) return exit_error("-s 1 is mandatory when -m");
    }
    else if ( args[i] == "-f" )
    {
      ++i;
      if ( i >= args.size() ) return exit_error("-f flag needs an argument");
      line_cutoff = std::stod( args[i] );
    }
    else if ( args[i] == "-v" )
    {
      profile = true;
    }
    else if ( args[i] == "-m" )
    {
      is_majorant = true;
      if (lines_per_leaf != 1) return exit_error("-s 1 is mandatory when -m");
    }
    else if ( args[i] == "-h" )
    {
      return false;
    }
    else if ( args[i] == "-c" )
    {
      ++i;
      if ( i >= args.size() ) return exit_error("-c flag needs an argument");
      concentration = std::stod( args[i] );
    }
    else if ( args[i] == "-t" )
    { 
      ++i; 
      temperature = std::stod(args[i]);
    }
    else if ( args[i] == "-e" )
    { 
      ++i;
      RDP_precision = std::stod(args[i]);
    }
    else if ( args[i] == "-p" )
    { 
      pressureflag = true; 
    }
    else if ( pressureflag )
    { 
      pressure.push_back( std::stod( args[i] ) ); 
    }
    else{ 
      return exit_error("unknown argument " + args[i]);
    }
  }
  
  // Errors handling
  if ( !pathflag ) return exit_error("base file and pressure need to be provided");
  if ( pressure.empty() ) return exit_error("pressure should be given");
  // pressure.push_back( 1 ); 
  if ( !nameflag ) return exit_error("tree name is mandatory");
 
  args_ = Args( input_data_file, output_tree_file, create, pressure, lines_per_leaf, line_cutoff, concentration, temperature, profile, children_per_node, RDP_precision, is_majorant );
  return true;
}

void parser::help() {
  std::cout
  << "Usage: buildProbaTree [OPTIONS]" << std::endl
  << std::endl
  << "Build a binary tree structure from a spectroscopic line-by-line database "
  << "to obtain both a majorant of the absorption coefficient and sample a transition / line. "
  << "Each tree is built for one species, and eventually several pressures."
  << std::endl
  << std::endl
  << "Mandatory options" << std::endl
  << " -b file.txt" << std::endl
  << "    File containing transitions." << std::endl
  << " -p [double...]" << std::endl
  << "    Set of pressures (in atm) at which the tree is built." << std::endl
  << "    At least one pressure is required." << std::endl
  << " -n string " << std::endl
  << "    Output tree file (.sav extension is added automatically)." << std::endl
  << "    A new file is created to store the tree." << std::endl
  << "    Incompatible with -a option." << std::endl
  << std::endl
  << "Other options" << std::endl
  /*
  << " -a string "<< std::endl
  << "    Input and output tree file, .sav extension is added automatically." << std::endl
  << "    The built tree is added to the specified tree." << std::endl
  << "    Incompatible with -n option." << std::endl
  */
  << " -f double" << std::endl
  << "    Transition line cutoff (default: "<< DEFAULT_LINE_CUTOFF <<")." << std::endl
  << " -c double " << std::endl
  << "    Specie concentration (in ...) at which the tree is built (default: "<< DEFAULT_CONCENTRATION <<")." << std::endl
  << " -t double" << std::endl
  << "    Temperature in Kelvin at which the tree is built (default: "<< DEFAULT_TEMPERATURE <<")." << std::endl
  << " -v " << std::endl
  << "    Flag to select Voigt profile, Lorentz profile by default." << std::endl
  << " -e double" << std::endl
  << "    Relative simplification error, Ramer-Douglas-Peucker parameter (default: "<< DEFAULT_SIMPLIFICATION_CRITERION <<")." << std::endl
  << " -m " << std::endl
  << "    Flag to impose a majorant tree (for the given thermodynamic conditions only)." << std::endl
  << "    Approximated majorant otherwise." << std::endl
  << "    Activates - a shift for each leaf (containing 1 transition) "<< std::endl
  << "              - the modified Ramer-Douglas-Peucker procedure (to ensure a majorant point simplification). "<< std::endl
  << std::endl
  << "Debug options [should not be used!]" << std::endl
  << " -k int" << std::endl
  << "    Number of children per cluster (default: 2, binary tree). " << std::endl
  << " -s int" << std::endl
  << "    Maximal number of transitions per leaf (default: 1)." << std::endl;
} 
