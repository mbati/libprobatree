/* Copyright (C) 2021-2024 Université Paul Sabatier, CNRS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */


#include "cluster.hpp"
#include "transition.h"

#include <chrono>
#include <iostream>
#include <cstring>
#include <stdexcept>

struct Range {
  double min = -1;
  double max = -1;
};


using namespace std;



// TODO: should print the absorption coefficient spectrum for different thermodynamic 
// conditions that the one used to build the tree. 
// It would help to visualize the interpolation performed by libProbaTree.

// TODO pressures are available in parser... 

void usage(std::string name) {
    std::cout<<"Usage: "<<std::endl
             <<name<<" absorption (-exact | -approx | -transitions) [OPTIONS]"<<std::endl
             <<"or alternatively "<<std::endl
             <<name<<" proba (-exact | -approx) [OPTIONS]"<<std::endl
             <<std::endl
             <<"Write data stored in the tree to a file, either the absorption coefficient spectrum "
             <<"or probabilities, for a specific node. "
             <<"Prints either the exact spectrum (i.e. computed from the transition database), or ''approximated'' (i.e. stored in the tree), or eventually the unit transitions in a node. "<<std::endl
             <<std::endl
             <<"Mandatory options"<<std::endl
             <<"  absorption | proba "<<std::endl
             <<"      Indicate whether the absorption coefficient spectrum or probabilies are extracted."<<std::endl
             <<"  -exact  "<<std::endl
             <<"      Print the exact spectrum."<<std::endl
             <<"  -approx  "<<std::endl
             <<"      Print the stored (approximated) spectrum."<<std::endl
             <<"  -transitions  "<<std::endl
             <<"      Dump the exact unit transitions."<<std::endl
             //  
             <<"  -tree  string"<<std::endl
             <<"      Input tree (.sav) file. "<<std::endl
             <<"  -output  string"<<std::endl
             <<"      Output filename. "<<std::endl
             <<std::endl
             <<"Other options"<<std::endl
             <<"  -node  int int"<<std::endl
             <<"      Node depth and child (default: root 0 0). "<<std::endl
             <<"      Numbering convention for children:        "<<std::endl
             <<"      depth 0         c=         0              "<<std::endl
             <<"                               /   \\           "<<std::endl
             <<"      depth 1         c=      0     1           "<<std::endl
             <<"                             / \\   / \\        "<<std::endl
             <<"      depth 2         c=    0   1 2   3         "<<std::endl
             <<"                                ...             "<<std::endl
             <<"  -range  double double"<<std::endl
             <<"      Wavenumber range to be considered (in cm-1), (default: node range). "<<std::endl
             <<"  -n  int"<<std::endl
             <<"      Number of wavenumber discretization steps in the range (first and last wavenumbers are included)."<<std::endl
             <<"      Incompatible with -approx option."<<std::endl;
}

void error(std::string msg) {
  usage("");
  throw std::invalid_argument(msg);
}

bool ternary_XOR(bool a, bool b, bool c) {
  return a ? !(b | c) : (b ^ c);
}

void parse_command_line(int argc, char const* argv[],
                        bool &print_absorption_coeff,
                        std::string& in_filename,
                        std::string& out_filename,
                        double& pressure,
                        double& w_min,
                        double& w_max,
                        int& depth,
                        int& child,
                        int& n_steps,
                        bool& print_approx_spectrum,
                        bool& print_exact_spectrum,
                        bool& print_unit_transitions_spectrum
                        )
{
  if (argc < 2) error("Please add arguments");
  print_exact_spectrum = false;
  print_approx_spectrum = false;
  print_unit_transitions_spectrum = false;
  in_filename=""; out_filename="";
  w_min = -1; w_max = -1;
  pressure=-1;
  depth = 0; child = 0;
  int i = 1;

  if (strcmp(argv[i], "absorption") == 0) { print_absorption_coeff = true; }
  else if (strcmp(argv[i], "proba") == 0) { print_absorption_coeff = false; }
  else error("Second argument should be absorption or proba.");
  i++; 

  while ( i < argc )
  {
    if (strcmp(argv[i],"-exact") == 0) { 
      print_exact_spectrum = true;
    }
    else if (strcmp(argv[i], "-approx") == 0) { 
      print_approx_spectrum = true;
    }
    else if (strcmp(argv[i], "-transitions") == 0) { 
      print_unit_transitions_spectrum = true;
      if (!print_absorption_coeff) error("Incompatible options proba and -transitions");
    }
    //  
    else if (strcmp(argv[i], "-h") == 0) { usage(argv[0]); exit(0); }
    else if (strcmp(argv[i], "-tree") == 0) {
      in_filename = argv[i+1];
      i+=1;
    }
    else if (strcmp(argv[i], "-output") == 0) {
      out_filename = argv[i+1];
      i+=1;
    }
    else if (strcmp(argv[i], "-n") == 0) {
      n_steps = std::stoi( argv[i+1] ); // TODO: should be size_t
      if (n_steps < 0) error("Please input a positive number of steps.");
      i+=1;
    }
    else if (strcmp(argv[i], "-p") == 0) {
      pressure = std::stod( argv[i+1] );
      if (pressure < 0. || pressure > 1.) error("Please input pressure in [0,1].");
      i+=1;
    }
    else if (strcmp(argv[i], "-range") == 0) {
      w_min = std::stod( argv[i+1] );
      w_max = std::stod( argv[i+2] );
      if (w_min < 0. || w_max < 0.) error("Please input a positive wavenumber.");
      i+=2;
    }
    else if (strcmp(argv[i], "-node") == 0) {
      depth = std::stoi( argv[i+1] );
      child = std::stoi( argv[i+2] );
      if (depth < 0 || child < 0) { //|| child > 2**depth) {
        error("Incorrect depth and child.");
      }
      i+=2;
    }
    else {
      std::string option = argv[i];
      error("Unrecognised option "+option);
    }
    i++; 
  }
  if (in_filename == "" || out_filename == "") {
    error("Please specify an input (.out) and an output (.txt) files");
  }
  if (print_absorption_coeff && ! ternary_XOR(print_exact_spectrum, print_approx_spectrum, print_unit_transitions_spectrum)) {
    error("-exact, -approx, -transitions are exclusive, one should be active."); 
  } else if ((! print_absorption_coeff) && !(print_exact_spectrum ^ print_approx_spectrum)) {
    error("-exact, -approx are exclusive, one should be active."); 
  }
}

void uniform_frequency_discretization(struct Range range,
                                      int N,
                                      std::vector<double>& nu,
                                      std::vector<double>& tr)
{
  if (range.max < range.min) error("Invalid spectral domain");
  const double space    = ( range.max - range.min ) / ( N -1 ); //-1 because we consider the last point
  const double nu_start = range.min;
  const double nu_end   = range.max;
  if (space < 0 || nu_start < 0 || nu_end < 0 || N < 0) error("Problem while discretizing spectral domain");
  nu.resize(N), tr.resize(N);
  
  for (int i=0; i<N; i++) nu[i] = nu_start + i*space;
}

// #include <algorithm>


// Select the points within the wavenumber range
// Return their value in x,y vectors TIMES the density provided
// Correctly add virtual points at boundaries to match the required range
// as interpolation between the points in the list
void get_spectrum(const std::vector<Point>& points,
                  struct Range range,
                  double density,
                  std::vector<double>& x,
                  std::vector<double>& y)
{
    x.clear(); y.clear();

    double mini = range.min;
    double maxi = range.max;

    // Select (first,last) indices of points that match the required range
    size_t n = points.size();
    size_t first = 0;
    while ( points[first].nu < mini ) first++;
    size_t last = n-1;
    while ( points[last].nu > maxi ) last--;

    n = last - first +1;
    x.resize( n );
    y.resize( n );

    // Fill the result approximation with data
    for (int i = 0; i < (int) n; i++) {
        x[i] = points[first + i].nu;
        y[i] = points[first + i].contrib;
    }

    // Add boundaries if needed to fit the required range
    // (if smaller than the data one)
    size_t last_i = last;
    auto lerp = []( double a, double b, double t ) { return ( 1 - t ) * b + t * a; };
    if (x[0] > mini) {
        x.insert(x.begin(), mini);
        y.insert(y.begin(),
                 lerp( points[first - 1].contrib, points[first].contrib,
                 ( points[first].nu - mini ) / ( points[first].nu - points[first - 1].nu ) ));
    }

    if (x[last_i] < maxi){
        x.insert(x.end(), maxi);
        y.insert(y.end(),
                lerp( points[last_i].contrib, points[last_i+1].contrib,
                     ( points[last_i+1].nu - maxi ) / ( points[last_i+1].nu - points[last_i].nu ) ) );
    }

    for (int i = 0; i < (int) y.size(); i++) y[i] *= density;
}



int main( int argc, char const* argv[] )
{
  std::string in_filename, out_filename;
  int n_steps = 0;
  bool print_absorption_coeff;
  bool print_approx_spectrum, print_unit_transitions_spectrum, print_exact_spectrum;
  double w_min, w_max;
  int depth, child;
  double pressure; 

  parse_command_line(argc, argv, 
                     print_absorption_coeff,
                     in_filename, out_filename,
                     pressure,
                     w_min, w_max, depth, child, n_steps,
                     print_approx_spectrum, 
                     print_exact_spectrum, 
                     print_unit_transitions_spectrum
                     );

  // Read tree
  std::fstream in_stream(in_filename);
  if (in_stream.fail())
  {
    error("Couldn't open file: "+in_filename+"\nPlease make sure it is an existing .sav file.");
  }
  load_molar_mass_file();
  SpectralProbabilityTrees tree = probatree_unserialize(in_stream);
  // TODO: we should call cbindings.h function instead

  // Select the node
  const Cluster* cluster = probatree_get_cluster(tree.species[0], depth, child);
  if (!cluster) error("Couldn't find node ");

  Specie s = tree.species[0];
  double pressure_index = 0;
  if (pressure == -1) {
    pressure = tree.pressures[pressure_index];
  }

  // TODO: all this should be extracted from library instead of using cluster.hpp
  double concentration = s.concentration; 
  double temperature = tree.temperature; 
  double line_cutoff = s.line_cutoff;
  bool voigt_profile = s.voigt_profile; 

  // Set default values
  if (w_min == -1) { w_min = cluster->trunc_left[pressure_index]; }
  if (w_max == -1) { w_max = cluster->trunc_right[pressure_index]; }
  Range range = {w_min, w_max};

  if (cluster->trunc_left[pressure_index] > w_max || cluster->trunc_right[pressure_index] < w_min)
  {
    std::string msg = "Incompatible wavenumber range ["+to_string(w_min)+" ; "+to_string(w_max)+"] and "
    "tree data ["+to_string(cluster->trunc_left[pressure_index])+", "+to_string(cluster->trunc_right[pressure_index])+"].";
    error(msg);
  }


  std::vector<double> nu, tr;


  if (print_absorption_coeff) 
  { 
    // We precompute density once
    double dst = density( concentration, pressure, temperature );

    // Compute transitions
    if (print_exact_spectrum)
    {
      uniform_frequency_discretization(range, n_steps, nu, tr);
      std::vector<Transition> transitions = probatree_get_transitions(*cluster);
      for ( int pt = 0; pt < n_steps; pt++ ) 
      {
        tr[pt] = dst * sum_of_h(transitions, nu[pt], pressure, concentration,
                                temperature, line_cutoff, voigt_profile);
      }
    }
    else if (print_unit_transitions_spectrum)
    {
      std::vector<Transition> transitions = probatree_get_transitions(*cluster);
      int nb_tr = transitions.size();
      for (int i = 0; i < nb_tr; i++) {
        std::vector<double> unit_nu, unit_tr;
        double nu_c = nu_center(&transitions[i], pressure);
        double mini = std::max(range.min, nu_c-line_cutoff);
        double maxi = std::min(range.max, nu_c+line_cutoff);
        uniform_frequency_discretization({mini, maxi}, n_steps, unit_nu, unit_tr);

        // Exact transition computation
        for ( int pt = 0; pt < n_steps; pt++ )
        {
          unit_tr[pt] = dst * h_transition(&transitions[i], unit_nu[pt], pressure, concentration, 
                                           temperature, line_cutoff, voigt_profile);
        }
        
        nu.insert(nu.end(), unit_nu.begin(), unit_nu.end());
        tr.insert(tr.end(), unit_tr.begin(), unit_tr.end());
      }
    }
    else if (print_approx_spectrum) 
    {
      // TODO: to be changed when interpolation will be added
      // now we give the exact polyline stored at the first pressure only.
      // The polyline may have no sense when interpolation will be considered.
      // Then the same strategy as below, i.e. spectrum discretization may be 
      // needed.

      // We do not extrapolate data:
      if (range.min < cluster->trunc_left[pressure_index])  range.min = cluster->trunc_left[pressure_index];
      if (range.max > cluster->trunc_right[pressure_index]) range.max = cluster->trunc_right[pressure_index];

      get_spectrum(cluster->points[pressure_index], range, dst, nu, tr);
    }
    

    // Export data
    std::ofstream out_stream(out_filename);
    out_stream.precision(10);
    int j = -1;
    for (int i = 0; i < (int) nu.size(); i++) {
      if (print_unit_transitions_spectrum && i >= (j+1)*n_steps) {
        j++;
        out_stream<<"#transition "<<j<<std::endl;
      }
      out_stream<<nu[i]<<"    "<<tr[i]<<std::endl;
    }
    out_stream.close();
  
  }
  else  /// Line probabilities
  {
    uniform_frequency_discretization(range, n_steps, nu, tr);
    std::vector<Transition> transitions = probatree_get_transitions(*cluster);
    int nb_tr = transitions.size();
    
    std::vector<double> unit_nu = nu; 
    std::vector<std::vector<double>> unit_tr;
    unit_tr.resize(nb_tr);
    for (int i = 0; i < nb_tr; i++) unit_tr[i].resize(n_steps);

    for ( int pt = 0; pt < n_steps; pt++ ) {
      std::vector<std::pair<Transition, double>> approx_probas;
      if (print_exact_spectrum) {
        transition_probabilities( transitions,
                                  nu[pt],
                                  pressure,
                                  s.concentration,
                                  temperature,
                                  s.line_cutoff,
                                  s.voigt_profile,
                                  approx_probas );
      }
      else if (print_approx_spectrum)
      {
        approximative_proba_cluster( *cluster,
                                     tree.pressures,
                                     nu[pt],
                                     pressure,
                                     concentration,
                                     temperature,
                                     1., // initial probability
                                     s.line_cutoff,
                                     s.voigt_profile,
                                     approx_probas );
      }

      for (int i = 0; i < nb_tr; i++ ) {
        unit_tr[approx_probas[i].first.id][pt] = approx_probas[i].second;
      }
    }
    /* 
    for (int i = 0; i < nb_tr; i++ ) {
        nu.insert(nu.end(), unit_nu.begin(), unit_nu.end());
        tr.insert(tr.end(), unit_tr[i].begin(), unit_tr[i].end());
    }*/

    // Export data
    std::ofstream out_stream(out_filename);
    out_stream.precision(10);
    // out_stream<<"#wavenumber probability "<<std::endl;
    for (int j = 0; j < nb_tr; j++) {
      out_stream<<"#transition "<<j<<std::endl;
      for (int i = 0; i < (int) nu.size(); i++) {
        out_stream<<nu[i]<<"    "<<unit_tr[j][i]<<std::endl;
      }
    }

    // FOR YANISS
    // std::ofstream out_stream(out_filename);
    // out_stream.precision(10);
    // for (int i = 0; i < (int) nu.size(); i++) {
    //   out_stream<<nu[i]<<" ";
    //   for (int j = 0; j < nb_of_transitions; j++) {
    //     out_stream<<tr[j][i]<<" ";
    //   }
    //   out_stream<<std::endl;
    // }
    
    out_stream.close();
  }

  // TODO, should have the same output format for both absorption spectrum and probability
  return 0;
}
