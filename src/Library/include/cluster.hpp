/* Copyright (C) 2021-2024 Université Paul Sabatier, CNRS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */


#pragma once

#include <fstream>
#include <random>
#include <vector>
#include <functional>

#include "transition.h"

#define DEFAULT_TEMPERATURE 296.0
#define UNSET -1

/*
 *		Structures
 */

struct Point {
  double nu;
  double contrib;

  Point(double nu=0, double contribution=0): nu(nu), contrib(contribution) {}
}; 

inline bool operator < (const Point& lhs, const Point& rhs) { return lhs.nu < rhs.nu; }

// Represent a node of the probability tree
struct Cluster {
  // wavenumber bounds on the cluster
  std::vector<double> trunc_left  {{ std::numeric_limits<double>::max() }};
  std::vector<double> trunc_right {{ std::numeric_limits<double>::min() }};
  // Hierarchical links
  Cluster* parent { nullptr };
  std::vector<Cluster> children;
  // transitions, sorted by nu_0
  std::vector<Transition> transitions;
  // Approximation of the probability density (approximation of the
  // sum of the transition contribution) for each pressure. Indexed
  // as the pressures array of the Tree
  std::vector<std::vector<Point>> points;
};

struct Specie {
  // line_cutoff used at build time
  double line_cutoff;
  // indicate if the tree is built using voigt profile
  bool voigt_profile;
  // concentration of the specie
  double concentration;
  // precomputation of the density
  //std::vector<double> density;
  // Maximal number of children for each cluster
  int max_child_nb;
  // Root of the proba tree for the specie
  Cluster root;
  // name of the specie
  std::string name;
  // transitions of the species
  std::vector<Transition> transitions;
};

struct SpectralProbabilityTrees {
  // Vector of probability tree for each species
  std::vector<Specie> species;
  // Vector of the precomputed pressures in each probability tree
  std::vector<double> pressures; /* MUST BE INCREASING */
  // Reference temperature for tree building (and usage)
  double temperature {UNSET};
};


/*
 *		Probability tree building functions
 */

/// Classify transitions and build the base probability tree.
/// @param transitions the transition set, expected to be sorted by tr.nu_c
/// @param k number of clusters for each subdivision
/// @param leaf_size number of transitions per cluster. This act as stopping
/// criterion on the tree building.
/// @param vg the statistics about the transitions returned by load_transitions
Specie classify_transitions( std::vector<Transition>&& transitions,
                             int k,
                             int leaf_size );

/// Process the tree to precompute the contribution function at each level. For
/// internal node, this function is approximated using a piecewise linear
/// function. For leaves, the contribution is computed exactly from the
/// transitions associated with the leaf.
/// @param tree the tree to precompute.
/// @param specie index of the specie tree to build
/// @param progress_cb the function to call each time a cluster is processed.
/// @param line_cutoff the bandwidth cutoff bound. Transition contributions are
/// clamped on the interval tr.nu_c +/- line_cutoff
/// @param voigt_profile indicates if the transitions must be computed from
/// Lorentz profile (parameter false, default) or with Voigt
/// profile (parameter true)
void probatree_compile( SpectralProbabilityTrees& tree,
                        unsigned int specie,
                        const std::function<void( int )>& progress_cb,
                        double line_cutoff,
                        double concentration,
                        bool voigt_profile,
                        double precision = 0.05,
                        bool majorant = true );


/*
 *	Save/load functions
 */

/// Write the tree on the output stream
void probatree_serialize( SpectralProbabilityTrees& tree, std::fstream& f );

/// Load the tree from the ouput stream
SpectralProbabilityTrees probatree_unserialize( std::fstream& f );


/*
 *  Main functions: access to tree data
 */

/// Compute the \hat{k} value of the tree for the given wavenumber, pressure and concentrations
double probatree_khat( const SpectralProbabilityTrees& tree,
                       double nu,
                       double P,
                       const std::vector<double>& concentrations,
                       double temperature);

/// Compute the \hat{k} value of the tree for the given wavenumber, pressure and concentrations
double probatree_k_exact( const SpectralProbabilityTrees& tree,
                          double nu,
                          double P,
                          const std::vector<double>& concentrations,
                          double temperature,
                          double line_cutoff,
                          bool voigt_profile );

std::mt19937::result_type getRand();


/// Seed the random number generator used by sampler
/// @param seed the seed to use
void probatree_sampler_seed( unsigned int seed );


/// Sample according to the approximation represented by the tree.
/// @param tree the probability tree to sample
/// @param nu the wavenumber
/// @param P the pressure to use. Must belong to the interval of pressures used to build the tree
/// @param concentrations the concentration of the gaz mixture to sample.
/// @param specie will contains the id of the specie sampled
/// @return a transition Id from the sampled specie with its probability
TransitionIdWithProba probatree_sample_transition( const SpectralProbabilityTrees& tree,
                                                   double nu,
                                                   double P,
                                                   const std::vector<double>& concentrations,
                                                   unsigned int& specie );


/*
*  Navigation in the tree structure
*/

/// Return the number of clusters within the tree
int probatree_nclusters( const SpectralProbabilityTrees& tree );

/*
/// Select the cluster at the position depth, child in the specie tree
/// For 2 children per cluster, the convention is the following:
///                          child
///   depth 0                  0
///                          /   \
///   depth 1               0     1
///                        / \   / \
///   depth 2             0   1 2   3
///                           ...
/// @param specie the considered tree
/// @param depth the cluster vertical level (0 for root)
/// @param child the cluster horizontal position (0 to K^(depth-1))
/// @return a cluster adress
*/
const Cluster* probatree_get_cluster(const Specie& specie, int depth, int child);


/// Return the list of transitions within a given cluster
/// @param cluster the root cluster
/// @return a vector of transitions
std::vector<Transition> probatree_get_transitions( const Cluster& cluster );


#define EXPORT_FOR_UNIT_TESTS
#ifdef EXPORT_FOR_UNIT_TESTS
double lerp( const std::vector<Point>& v, double x );
#endif




TransitionIdWithProba transition_sampler_proba_exact( const SpectralProbabilityTrees& tree,
                                                      double nu,
                                                      double P,
                                                      const std::vector<double>& concentrations,
                                                      unsigned int& specie );

/// Return the pair of <transition, approx_proba> for all transitions,
/// in cluster c
void approximative_proba_cluster( const Cluster& c,
                                  const std::vector<double>& pressures,
                                  double nu,
                                  double P,
                                  double C,
                                  double T,
                                  double proba,
                                  int line_cutoff,
                                  bool voigt_profile,
                                  std::vector<std::pair<Transition, double>>& allProbas );
