/* Copyright (C) 2021-2024 Université Paul Sabatier, CNRS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */


#pragma once

#ifndef __cplusplus
#include <stdbool.h>
typedef struct Transition Transition;
typedef struct TransitionIdWithProba TransitionIdWithProba;
typedef struct PhysicalConstants PhysicalConstants;
#endif

#ifdef __cplusplus
extern "C"
{
#endif


// Shift central wavenumber ccording to current pressure 
double spectro_nu_center(const double nu_0, const double shift, const double P);

/// Compute the half-width-at-half-height of a Lorentzian distribution
double spectro_gamma_lorentz(const double gamma_air, const double gamma_self, 
                                    const double n_air, const double pressure, 
                                    const double partial_pressure, const double temperature);

/// Compute the half-width-at-half-height of a Doppler distribution
double spectro_gamma_doppler(double nu_center, double temperature, double molar_mass);

/// Lorentz profile, where diff is difference between wavenumber and central wavenumber
double spectro_lorentz_profile(double gamma, double diff);

/// Voigt profile: auto compute gamma and evaluate Voigt 
double spectro_voigt_profile(double wvnb, double nu_center, double molar_mass, 
                                    double temperature, double gamma_L);

/// Compute the moleclar density from the concentration, pressure and temperature
double spectro_density(const double partial_pressure, const double temperature);

/// Call BD_TIPS
double spectro_get_Q(int molec_id, int iso_id, double temperature);

/// 
double spectro_intensity(const int molec_id,
                         const int iso_id,
                         const double intensity,
                         const double E_low,
                         double temperature,
                         double Q_T_ref,
                         double n_c);

/// Load the molar mass file from Hitran
void load_molar_mass_file();

// Return molar mass in kg 
double get_molar_mass(int molecule_index, int isotopologue_index);

/* Purpose: to calculate the Faddeeva function with relative error less than
 * 10^(-4).
 *
 * Inputs: x and y, parameters for the Voigt function :
 * - x is defined as x=(nu-nu_c)/gamma_D*sqrt(ln(2)) with nu the current
 *   wavenumber, nu_c the wavenumber at line center, gamma_D the Doppler
 *   linewidth.
 * - y is defined as y=gamma_L/gamma_D*sqrt(ln(2)) with gamma_L the Lorentz
 *   linewith and gamma_D the Doppler linewidth
 *
 * Output: k, the Voigt function; it has to be multiplied by
 * sqrt(ln(2)/pi)*1/gamma_D so that the result may be interpretable in terms of
 * line profile.
 *
 * TODO check the copyright */
//extern double sln_faddeeva(const double x, const double Y);
double sln_faddeeva(const double x, const double Y);



/*
 *		Structures
 */

struct PhysicalConstants {
  double Na; // Avogadro constant in mol^{-1}
  double Kb; // Boltzmann constant in m^2 kg s^{-2} K^{-1}
  double c;  // Speed of light in m s^{-1}
  double R;  // Gaz contant in m^3 atm mol^{-1} K^{-1}
};

struct Transition {
  int molec_id;      // molecule ID in [1,49]
  int iso_id;        // isotopologue (local numbering)
  double molar_mass; // store molar mass in kg  
  double Q_T_ref;    // precomputation: store Q at ref temperature 
  double nu_0;       // central wavenumber (in cm-1)
  double intensity;  // line intensity (multiplied by abundance) at T=296K (in cm-1/(molecule cm-2))
  double gamma_air;  // air-broadened half width at half maximum (HWHM) p=1atm and T=296K (in cm-1/atm)
  double gamma_self; // self-broadened HWHM at P=1atm and T=296 (in cm-1/atm)
  double E_low;      // lower state energy of the transition in cm-1
  double n_air;      // coefficient of the temperature dependence of the air-broadened half width
  double shift;      // delta_air - pressure shift induced by air, referred to P=1atm (in cm-1/atm)
  int id;            // transition number (line number in file)
};

struct TransitionIdWithProba {
  int id;
  double nu_c;
  double proba;
};



/*
 *		Formulas to compute the absorption coefficient from a transition
 */

extern const PhysicalConstants physical_constants;

// Initialize a transition with some precomputations (molar mass, Q_T_ref, etc
Transition init_transition(const int molec_id, const int iso_id, const double nu_0, 
                           const double intensity, const double gamma_air, 
                           const double gamma_self, const double E_low, const double n_air, 
                           const double shift, const int id); 


// Approximation of Voigt half-width-at-half-height
double approx_gamma_voigt(const Transition* transition,
                          double pressure,
                          double concentration,
                          double temperature);

/// Compute the contribution of a transition tr at a given wavenumber nu,
/// under the thermodynamical conditions of pressure, concentration, temperature.
/// The transition tails are set to zero at a distance line_cutoff from the central wavenumber
/// The contribution is in meter^{-1}
double h_transition(const Transition* tr,
                    double nu,
                    double pressure,
                    double concentration,
                    double temperature,
                    double line_cutoff,
                    bool voigt_profile);


#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
#include <string>
#include <vector>
#include <cmath>

/// /* TODO: to delete
double gamma_lorentz(const Transition* transition,
                     double pressure, double concentration, double temperature);

/// Shifted central wavenumber according to actual pressure
double nu_center(const Transition* tr, double pressure);

/// Compute the moleclar density from the concentration, pressure and temperature
double density(double concentration, double pressure, double temperature);
/// */

/// Get the molecule name from its index m
std::string get_molecule_name(int molecule_index);

/// Load a set of transitions from a file
/// @param path the file to load
/// @param transitions the vector to fill with loaded transitions
void load_transitions(const std::string& path,
                      std::vector<Transition>& transitions);

/// Sum the h contribution of several transitions
double sum_of_h(const std::vector<Transition> &transitions,
                double nu,
                double pressure,
                double concentration,
                double temperature,
                double line_cutoff,
                bool voigt_profile);

/// Compute the absorption coefficient (density * h_transition)
/// at the thermodynamical conditions
double absorption_coefficient(const std::vector<Transition>& transitions,
                              double nu,
                              double pressure,
                              double concentration,
                              double temperature,
                              double line_cutoff,
                              bool voigt_profile);

/// Return a list< of pairs of (transition and probability) >
/// under the given thermo conditions
void transition_probabilities(const std::vector<Transition>& transitions,
                              double nu,
                              double pressure,
                              double concentration,
                              double temperature,
                              double line_cutoff,
                              bool voigt_profile,
                              std::vector<std::pair<Transition, double>>& allWeights);

#endif
