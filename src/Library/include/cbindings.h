/* Copyright (C) 2021-2024 Université Paul Sabatier, CNRS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */


#pragma once

#include "transition.h"

#ifdef __cplusplus
extern "C"
{
#endif

  /* Public type for spectral probability tree */
  typedef struct ClusterSampler ClusterSampler;

  /* Loads a spectral probability tree from the given file */
  ClusterSampler* ClusterSampler_create( const char* path );

  /* Free the memory used by the given spectral probability tree */
  void ClusterSampler_destroy( ClusterSampler* sampler );

  /* Sample a transition globally using the probability tree.
   * The size of  concentration array must match the number of species in the tree.
   * return the sampled transition with its probability and the specie from which
   * the transition was sampled
   */
  TransitionIdWithProba ClusterSampler_sample_transition( ClusterSampler* sampler,
                                                          double nu,
                                                          double P,
                                                          double* concentrations,
                                                          unsigned int* specie_sampled );

  /* Compute the approximated kmax from the tree for the given specie.
   * the size of  concentration array must match the number of species in the tree.
   */
  double ClusterSampler_kmax( ClusterSampler* sampler,
                              double nu,
                              double P,
                              double* concentrations,
                              double temperature);

  double ClusterSampler_k_exact( ClusterSampler* sampler,
                                 double nu,
                                 double P,
                                 double* concentrations,
                                 double temperature,
                                 double line_cutoff,
                                 int voigt_profile );

#ifdef __cplusplus
}
#endif
