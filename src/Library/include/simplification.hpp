/* Copyright (C) 2021-2024 Université Paul Sabatier, CNRS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */


#pragma once

#include <vector>
#include "cluster.hpp" // for Point definition

// Simplify the given polyligne in points, with an error lower than epsilon
// using the classical Ramer-Douglas-Peucker method by default
// or a custom majorant version of this method if majorant is true
double simplify( std::vector<Point>& points, double precision, bool majorant );


#ifdef TESTS
// The following functions are described in the .cpp file

double slope(const Point& p_start,
             const Point& p_end);

double signed_linear_approx_distance( const Point& refPoint,
                                      const Point& p,
                                      double s);

void find_max_error_point( int istart, int iend,
                           const std::vector<Point>& points,
                           int& s_imax, double& s_dmax,
                           int& u_imax, double& u_dmax);

std::vector<Point>
RDP_line_simplification( std::vector<Point> points,
                         double eps,
                         bool majorant_RDP,
                         double& error_max );
#endif
