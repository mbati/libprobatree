/* Copyright (C) 2021-2024 Université Paul Sabatier, CNRS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */


#pragma once

#include "cluster.hpp" // Needed for Point definition
#include "transition.h"
#include <vector>

/// Computes in @param points a majorant of the provided
/// @params transitions under thermo conditions
/// Also return
void build_leaf_majorant(const std::vector<Transition> &transitions,
                         double pressure,
                         double concentration,
                         double temperature,
                         double line_cutoff,
                         bool voigt_profile,
                         double precision,
                         bool majorant,
                         std::vector<Point> &points,
                         double &trunc_left,
                         double &trunc_right);
