/* Copyright (C) 2021-2024 Université Paul Sabatier, CNRS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */


#include "cbindings.h"
#include "cluster.hpp"

#include <iostream>
#include <random>


ClusterSampler* ClusterSampler_create( const char* path )
{
  std::fstream f( path );
  if ( f.fail() )
  {
    std::cerr << "Couldn't open file: " << path << std::endl;
    return nullptr;
  }

  load_molar_mass_file();
  
  auto tree = new SpectralProbabilityTrees;
  *tree     = probatree_unserialize( f );

  // std::cout << "Tree loaded for specie " << get_molecule_name(tree->transitions[0].m) << std::endl;
  // std::cout << "\t\tpressure : " << tree->pressure << std::endl;
  // std::cout << "\t\tconcentration : " << tree->concentration << std::endl;
  // std::cout << "\t\ttemperature : " << tree->temperature << std::endl;
  // std::cout << "\t\tprofile : " << ( tree->voigt_profile ? "Voigt" : "Lorentz" ) << std::endl;
  // std::cout << "\t\tcutoff : " << tree->line_cutoff << std::endl;
  // std::cout << "\t\tnumber of transitions : " << tree->transitions.size() << std::endl;

  return reinterpret_cast<ClusterSampler*>( tree );
}


void ClusterSampler_destroy( ClusterSampler* sampler )
{
  auto tree = reinterpret_cast<SpectralProbabilityTrees*>( sampler );
  //clear_molar_mass_data();
  delete tree;
}


double ClusterSampler_kmax( ClusterSampler* sampler,
                            double nu,
                            double P,
                            double* concentrations,
                            double temperature)
{
  auto tree = reinterpret_cast<SpectralProbabilityTrees*>( sampler );

  std::vector<double> con_vec( concentrations, concentrations + tree->species.size() );
  return probatree_khat( *tree, nu, P, con_vec, temperature );
}


double ClusterSampler_k_exact( ClusterSampler* sampler,
                               double nu,
                               double P,
                               double* concentrations,
                               double temperature,
                               double line_cutoff,
                               int voigt_profile )
{
  auto tree = reinterpret_cast<SpectralProbabilityTrees*>( sampler );

  std::vector<double> con_vec( concentrations, concentrations + tree->species.size() );
  bool vp = voigt_profile==0? true : false;
  return probatree_k_exact( *tree, nu, P, con_vec, temperature, line_cutoff, vp );
}


TransitionIdWithProba ClusterSampler_sample_transition( ClusterSampler* sampler,
                                                        double nu,
                                                        double P,
                                                        double* concentrations,
                                                        unsigned int* specie_sampled )
{
  auto tree = reinterpret_cast<SpectralProbabilityTrees*>( sampler );
  // TODO make sure the required concentration is the same as the tree one
  std::vector<double> con_vec( concentrations, concentrations + tree->species.size() );
  return probatree_sample_transition( *tree, nu, P, con_vec, *specie_sampled );
}
