/* Copyright (C) 2021-2024 Université Paul Sabatier, CNRS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */


#include "cluster.hpp"
#include "majorant.hpp"
#include "simplification.hpp"

#include <iostream>
#include <queue>
#include <cassert>
#include <algorithm>

#ifdef HAS_PARALLEL_POLICY
#include <execution>
#endif


// Subdivide the given cluster into k sub_clusters according to the given cost function
void frequency_clustering( Cluster* c,
                           size_t nb_child_per_cluster)
{
  int size = c->transitions.size();
  int transitions_per_cluster = (int)std::round((double)size / (double) nb_child_per_cluster);

  c->children.resize( nb_child_per_cluster );

  #ifdef ACTIVE_PARALLEL_COMPUTATION
    #pragma omp parallel for
  #endif 
  for (int k = 0; k < (int) nb_child_per_cluster; k++) {
    for (int t = k*transitions_per_cluster; t < std::min((k+1)*transitions_per_cluster, size); t++) {
      c->children[k].transitions.push_back( c->transitions[t] );
    }
  }
  c->transitions.clear();
}


// Classify transitions in a K-tree with at most leaf_size transitions per leaf
// They are classified according to their central frequency
Specie classify_transitions( std::vector<Transition>&& transitions,
                             int k,
                             int leaf_size )
{
  Specie s;
  s.max_child_nb = k;
  s.transitions  = transitions;

  std::queue<Cluster*> unexplored_clusters;
  s.root.parent      = nullptr;
  s.root.transitions = s.transitions;

  unexplored_clusters.push( &s.root );

  while ( !unexplored_clusters.empty() )
  {
    Cluster* c = unexplored_clusters.front();
    unexplored_clusters.pop();

    if ( c->transitions.size() > static_cast<unsigned int>( leaf_size ) )
    {

      frequency_clustering( c, k );
      for ( unsigned int i = 0; i < c->children.size(); i++ )
      {
        c->children[i].parent = c;
        unexplored_clusters.push( c->children.data() + i );
      }
    }
  }

  return s;
}

/* linear interpolation at the value x between two surrounding points in v */
double lerp( const std::vector<Point>& v, double x )
{
  assert( !v.empty() );
  if (x == v[0].nu) return v[0].contrib; // Special case, not handled by the next lines

  /* find the two points surrounding the desired x */
  auto it = std::lower_bound(v.begin(), v.end(), x);

  if ( it == v.begin() || it == v.end() ) return 0.;

  /* position "it" on the point before the desired x */
  it--;
  assert( x >= ( *it ).nu && ( *( it + 1 ) ).nu >= x );

  /* Compute the interpolation factor depending on the two points [*it and *(it+1)] x values,
   * and the desired x */
  double t = ( x - ( *it ).nu ) / ( ( *( it + 1 ) ).nu - ( *it ).nu );
  assert( t >= 0 && t <= 1 );
  
  /* Linear interpolation of y */
  return t * ( *( it + 1 ) ).contrib + ( 1 - t ) * ( *it ).contrib;
}


// Return the approximation within the c cluster at the wavenumber nu
// as the linear interpolation between two points (for neighbouring wavenumbers)
// for the given pressure index.
double contribution_approx( const Cluster& c,
                            double nu,
                            size_t pressure_index )
{
  if ( nu < c.trunc_left[pressure_index] || nu > c.trunc_right[pressure_index] )
    return 0.;
  return lerp( c.points.at( pressure_index ), nu );
}

bool is_equal(double a, double b, double epsilon=1e-14) {
  return (fabs(a - b) < epsilon);
}

// Return the approximation wihtin the cluster c at the wavenumber nu
// as the linear interpolation between neighbouring wavenumbers and pressures.
double contribution_approx( const Cluster& c,
                            const std::vector<double>& pressures,
                            double nu,
                            double P )
{
  // if ( nu < c.trunc_left || nu > c.trunc_right ) return 0.;
  // Special case if single pressure
  if ( pressures.size() == 1 || is_equal(pressures[0], P) ) { return lerp( c.points.at( 0 ), nu ); }

  auto lb = std::lower_bound( pressures.begin(), pressures.end(), P );
  int iP = std::distance( pressures.begin(), lb );

  if ( iP < 0 || iP >= int(pressures.size()) )
  {
    std::cerr << "Requesting a contribution for the pressure " << P << " out of pressure range ["
              << pressures[0] << ", " << pressures[pressures.size() - 1] << "]. Abort."
              << std::endl;
    abort();
  }

  if ( iP == 0 ) { /* the pressure is lower than the ones stored */
    return lerp( c.points.at( 0 ), nu );
  } 
  if ( is_equal(pressures[iP], P) ) { return lerp( c.points.at( iP ), nu ); }

  assert( c.points.size() == pressures.size() );
  assert(iP-1 >= 0 && iP <= pressures.size());

  double v1 = lerp( c.points.at( iP - 1 ), nu );
  double v2 = lerp( c.points.at( iP ), nu );

  double P1 = pressures[iP - 1];
  double P2 = pressures[iP];

  double t = ( P - P1 ) / ( P2 - P1 );
  return t * v2 + ( 1. - t ) * v1;
}


// Precompute the majorant absorption coefficient on one leaf (polyline)
// at the given thermo conditions.
// Majorant means that a shift will be applied to ensure the majorant property.
void precompute_leaf_contributions( Cluster* node,
                                    const std::vector<double>& pressures,
                                    double concentration,
                                    double temperature,
                                    double line_cutoff,
                                    bool voigt_profile,
                                    const std::function<void()>& progress_cb,
                                    double precision,
                                    bool majorant)
{
  node->trunc_left.reserve(pressures.size());
  node->trunc_right.reserve(pressures.size());

  for ( int i = 0; i < (int) pressures.size(); i++ )
  {
    if ( !node->transitions.empty() )
    {
      std::vector<Point> points;
      build_leaf_majorant(node->transitions, pressures[i], concentration, temperature,
                          line_cutoff, voigt_profile, precision, majorant,
                          points, node->trunc_left[i], node->trunc_right[i]);

      node->points.push_back( points );
      progress_cb();
    }
    else
    { node->points.emplace_back( 0 ); }
  }
}


// Precompute the approximation on the internal nodes,
// given some thermo conditions.
// The polylines in the children are evaluated at different wavenumbers.
// For all points in a child's polyline, we add/compute the contribution of the others at this wavenumber.
// Then, we simplify the resulting polyline depending on the boolean apply_simplification.
void precompute_node_contributions( Cluster* node,
                                    const std::vector<double>& pressures,
                                    double concentration,
                                    double temperature,
                                    double line_cutoff,
                                    bool voigt_profile,
                                    const std::function<void()>& progress_cb,
                                    double precision,
                                    bool majorant )
{
  (void) concentration, (void) temperature, (void) line_cutoff, (void) voigt_profile;
  size_t pressure_index { 0 };
  node->trunc_left.reserve(pressures.size());
  node->trunc_right.reserve(pressures.size());

  for ( size_t p = 0; p < pressures.size(); p++ )
  {
    // Set the wavenumber domain in the node
    node->trunc_left[p] =
        std::accumulate( node->children.begin(),
                         node->children.end(),
                         std::numeric_limits<double>::max(),
                         [p]( double l, const Cluster& c ) { return std::min( l, c.trunc_left[p] ); } );
    node->trunc_right[p] =
        std::accumulate( node->children.begin(),
                         node->children.end(),
                         std::numeric_limits<double>::min(),
                         [p]( double l, const Cluster& c ) { return std::max( l, c.trunc_right[p] ); } );

    // 1. Sum the polyline approximations in all children

    // Create the structure containing all the current polyline points
    size_t nbpoints = std::accumulate( node->children.begin(),
                                       node->children.end(),
                                       size_t( 0 ),
                                       [pressure_index]( size_t n, const Cluster& c ) {
                                         return n + c.points[pressure_index].size();
                                       } );
    std::vector<Point> points( nbpoints );
    size_t i = 0;

    // Iterate over children. 
    // Polyligne pour le parent en commant sur les fils
    for ( size_t n = 0; n < node->children.size(); n++ )
    {
      // Iterate over children polyline points.
      #ifdef ACTIVE_PARALLEL_COMPUTATION
        #pragma omp parallel for schedule( static, 1 ) default( none ) \
          shared( i, n, node, points, pressure_index, line_cutoff, concentration, temperature, voigt_profile )
      #endif
      for ( size_t j = 0; j < node->children[n].points[pressure_index].size(); j++ )
      {
        // Initialize the result with the previously computed value.
        points[i + j] = node->children[n].points[pressure_index][j];

        // Iterate over the OTHER children,
        for ( size_t o = 0; o < node->children.size(); o++ )
        {
          if ( o != n ) // excluding the current child n (accounted twice otherwise).
          {
             // Add the other child's contribution at this wavenumber,
            // obtained using the majorant polyline.
            points[i + j].contrib += contribution_approx( node->children[o], points[i + j].nu, pressure_index );
          }
        }
      }
      i += node->children[n].points[pressure_index].size();
    }

    // 2. Sort the point vector according to the wavenumber
    // std::sort( points.begin(), points.end(), []( Point p1, Point p2 ) { return p1.nu < p2.nu; } );
    std::sort( points.begin(), points.end() );

    // 3. Apply simplification (point decimation)
    double error_max = simplify( points, precision, majorant );
    (void) error_max; // TODO: could be usefull in return for the user

    // 4. Save this new polyline in the current node!
    node->points.push_back( points );
    ++pressure_index;
    progress_cb();
  }

}

// Return the child index within the children list or -1 if not in the list
int child_index( Cluster* child, const std::vector<Cluster>& children )
{
  for ( int c = 0; c < int( children.size() ); c++ )
    if ( child == children.data() + c )
      return c;
  return -1;
};


// Compute all the tree nodes,
// calling the function progress_cb, each time a node computation is done.
// Post-order traversal:
// We need to visit first the leaves to compute their approximation,
// Then we go up in the tree and compute a node, once all its children are done
void probatree_compile( SpectralProbabilityTrees& tree,
                        unsigned int specie,
                        const std::function<void( int )>& progress_cb,
                        double line_cutoff,
                        double concentration,
                        bool voigt_profile,
                        double precision,
                        bool majorant )
{
  assert(is_sorted(tree.pressures.begin(), tree.pressures.end())); 
  Specie& s = tree.species[specie];
  if (tree.temperature == UNSET || tree.pressures.empty() )  {
    std::cout << "Error: tree temperature and pressures should be set" << std::endl;
    return;
  }
  s.line_cutoff   = line_cutoff;
  s.concentration = concentration;
  s.voigt_profile = voigt_profile;
  /*for (int i = 0; i < tree.pressures.size(); i++)
    s.density.push_back(density(concentration, tree.pressures[i], tree.temperature));
  */
  // verify the root<->children relations
  /*std::cerr << "Fixing root->child relation. (Need to fix C++ constructor of non pod objects)"
            << std::endl;*/
  for ( int i = 0; i < int( s.root.children.size() ); i++ )
  { s.root.children[i].parent = &s.root; }

  int done = 0; // Counts the number of compiled nodes

  // We start at the root and go down until we reach a leaf.
  // We compute this leaf first, then its "brothers".
  // Once done, their parent can be computed.
  // And we go up, until the root!
  Cluster* current = &s.root;
  Cluster* next    = current->parent;
  Cluster* prev    = current->parent;
  while ( current != nullptr )
  {
    // first time on the node prev, goto left children
    if ( prev == current->parent )
    {
      if ( current->children.empty() ) { next = nullptr; }
      else { next = current->children.data(); }
    }
    // search the son we come from
    int child_idx = child_index( prev, current->children );
    int last_child_idx = int(current->children.size()) - 1;
    if (next == nullptr || (child_idx >= 0 && child_idx < last_child_idx))
    {
      // no child or goto next child
      if ( current->children.empty() )  next = nullptr;
      else next = current->children.data() + child_idx + 1;
    }
    if ( next == nullptr || child_idx == last_child_idx )
    {
      // last visit of the node, compute data and goto parent
      if ( current->children.empty() )
      {
        /* Processing the data into the leaf */
        precompute_leaf_contributions( current,
                                       tree.pressures,
                                       s.concentration,
                                       tree.temperature,
                                       s.line_cutoff,
                                       s.voigt_profile,
                                       [&done, progress_cb]() { progress_cb( ++done ); },
                                       precision,
                                       majorant );
        /* end processing */
      }
      else
      {
        /* Processing internal node : Aggregating data from childrens */
        precompute_node_contributions( current,
                                       tree.pressures,
                                       s.concentration,
                                       tree.temperature,
                                       s.line_cutoff,
                                       s.voigt_profile,
                                       [&done, progress_cb]() { progress_cb( ++done ); },
                                       precision,
                                       majorant );
        /* end processing */
      }
      next = current->parent;
    }

    // continue the tree visit
    prev    = current;
    current = next;
  }
}


/* -------------------------------------------
              Structure navigation
   ------------------------------------------- */

// Number of clusters in a cluster
int nclusters( const Cluster& c )
{
 int n = std::accumulate( c.children.begin(), c.children.end(), 1,
                          []( int v, const Cluster& c ) { return v + nclusters(c); } );
 return n;
}


// Number of clusters in the tree
int probatree_nclusters( const SpectralProbabilityTrees& tree )
{
  int n = 0;
  for (const Specie& c : tree.species) n += nclusters(c.root);
  return n;
}


// Get the cluster at a specific position in the tree
const Cluster* probatree_get_cluster(const Specie& specie,
                                     int depth,
                                     int child )
{
  // Ealy exits
  if (depth == 0) return &specie.root;
  if (child >= pow(specie.max_child_nb, depth)) {
    std::cerr<<"Incompatible depth ("<<depth<<") and child ("<<child<<"): "
             <<"child should be < "<<pow(specie.max_child_nb, depth)<<std::endl;
    exit(EXIT_FAILURE);
  }

  // Iterate over children in the tree to find the expected one
  const Cluster* cl = &specie.root; // current selected cluster
  int d = 0;         // current depth in the tree
  int c = child;     // helps to compute the child to choose
  while (d < depth)
  {
    int colspan = pow(specie.max_child_nb, depth -d -1);
    int j = c/colspan;
    //std::cout<<"depth "<<d<<" child "<<j<<std::endl;
    cl = &cl->children[j];
    if (!cl) {
      std::cerr<<"No child at depth ("<<depth<<") and position ("<<child<<") "<<std::endl;
      exit(EXIT_FAILURE);
    }
    c -= j*colspan;
    d++;
  }
  return cl;
}


// Return transitions in a given cluster
std::vector<Transition> probatree_get_transitions( const Cluster& cluster )
{
  std::vector<Transition> res;
  std::queue<const Cluster*> unexplored_clusters;

  unexplored_clusters.push( &cluster );

  while ( !unexplored_clusters.empty() )
  {
    const Cluster* c = unexplored_clusters.front();
    unexplored_clusters.pop();

    if ( c->transitions.size() > 0 )
    { std::copy( c->transitions.begin(), c->transitions.end(), std::back_inserter( res ) ); }
    else
    {
      for ( unsigned int i = 0; i < c->children.size(); i++ )
      { unexplored_clusters.push( c->children.data() + i ); }
    }
  }
  std::sort(
      res.begin(), res.end(), []( const Transition& a, const Transition& b ) { return a.nu_0 < b.nu_0; } );
  return res;
}



/* -------------------------------------------
      Absorption coefficient approximation
   ------------------------------------------- */

double probatree_khat( const SpectralProbabilityTrees& tree,
                       double nu,
                       double P,
                       const std::vector<double>& concentrations,
                       double temperature)
{
  // TODO: the concentration may not be the one in the tree
  assert( tree.species.size() == concentrations.size() );
  double sum = 0;

  for ( unsigned int i = 0; i < tree.species.size(); i++ )
  {
    sum += density( concentrations[i], P, temperature )
         * contribution_approx( tree.species[i].root, tree.pressures, nu, P );
  }
  return sum;
}


double probatree_k_exact( const SpectralProbabilityTrees& tree,
                          double nu,
                          double P,
                          const std::vector<double>& concentrations,
                          double temperature,
                          double line_cutoff,
                          bool voigt_profile )
{
  // TODO: the concentration may not be the one in the tree
  assert( tree.species.size() == concentrations.size() );
  double sum = 0;

  for ( unsigned int i = 0; i < tree.species.size(); i++ )
  {
    sum += absorption_coefficient(tree.species[i].transitions,
                                  nu, P, concentrations[i],
                                  temperature, line_cutoff,
                                  voigt_profile);
  }
  return sum;
}

/* -------------------------------------------
      Approximated transition probability
   ------------------------------------------- */

void approximative_proba_cluster( const Cluster& c,
                                  const std::vector<double>& pressures,
                                  double nu,
                                  double P,
                                  double C,
                                  double T,
                                  double proba,
                                  int line_cutoff,
                                  bool voigt_profile,
                                  std::vector<std::pair<Transition, double>>& allProbas )
{
  if ( c.children.empty() ) // Leaf
  {
    if ( proba == 0. )
    {
      // all transitions will have a probability of 0
      // do not compute the contribution as it is not needed ...
      std::transform( c.transitions.begin(), c.transitions.end(),
                      std::back_inserter( allProbas ),
                      []( const Transition& tr ) { return std::make_pair( tr, 0. ); } );
    }
    else
    {
      // Compute the probabilities for all transitions in the leaf
      std::vector<double> weights;
      weights.reserve( c.transitions.size() );

      std::transform( c.transitions.begin(), c.transitions.end(),
                      std::back_inserter( weights ),
                      [nu, P, C, T, line_cutoff, voigt_profile]( const Transition& tr ) {
                        return h_transition( &tr, nu, P, C, T, line_cutoff, voigt_profile );
                      } );
      std::discrete_distribution<int> d( weights.begin(), weights.end() );
      std::vector<double> node_probas = d.probabilities();
      for ( int i = 0; i < int( c.transitions.size() ); i++ ) {
        double w = node_probas[i];
        if ( std::isnan( w ) ) { w = 0.; }
        allProbas.emplace_back( c.transitions[i], proba * w );
      }
    }
  }
  else
  {
    if ( proba == 0. )
    {
      // Go deeper in the tree
      for ( const Cluster& cls : c.children ) {
        approximative_proba_cluster(cls, pressures, nu,
                                    P, C, T, 0.,
                                    line_cutoff, voigt_profile, allProbas );
      }
    }
    else
    {
      std::vector<double> weights;
      weights.reserve( c.children.size() );
      std::transform( c.children.begin(), c.children.end(),
                      std::back_inserter( weights ),
                      [&pressures, P, nu]( const Cluster& child ) {
                        return contribution_approx( child, pressures, nu, P );
                      } );

      std::discrete_distribution<int> d( weights.begin(), weights.end() );
      std::vector<double> node_probas = d.probabilities();
      for ( int i = 0; i < int( c.children.size() ); i++ ) {
        double w = node_probas[i];
        if ( std::isnan( w ) ) { w = 0.; }
        approximative_proba_cluster(c.children[i], pressures, nu,
                                    P, C, T, proba * w,
                                    line_cutoff, voigt_profile, allProbas );
      }
    }
  }
}



/* -------------------------------------------
     Transition approximated sampling
  ------------------------------------------- */

// TODO check if OK on windows
// https://stackoverflow.com/questions/21237905/how-do-i-generate-thread-safe-uniform-random-numbers
static thread_local std::mt19937 globalRng( 0 );

// Seed the sampler
void probatree_sampler_seed( unsigned int seed )
{
  globalRng.seed( seed );
}

// This function is for test of multi-thread support
std::mt19937::result_type getRand()
{
  return globalRng();
}


/// Sample a specie from the tree using approximated probabilities.
/// @param tree the tree to sample
/// @param nu, P, concentrations the properties of the mixture to sample
/// @param s will be modified to get the sampled specie
/// @param C will be modified to get the concentration of the sampled specie
/// @return the probability of the sampled specie.
double probatree_specie_sampler( const SpectralProbabilityTrees& tree,
                                 double nu,
                                 double P,
                                 const std::vector<double>& concentrations,
                                 unsigned int& s,
                                 double& C )
{
  if (tree.species.size() == 1)
  {
    // Early exit if single specie
    s = 0; C = concentrations[0]; return 1.0;
  }

  /// This select the specie according to the concentration
  std::vector<double> weights;
  weights.reserve( tree.species.size() );

  std::transform( tree.species.begin(),
                  tree.species.end(),
                  concentrations.begin(),
                  std::back_inserter( weights ),
                  [&tree, P, nu]( const Specie& s, const double& ccn ) {
                    return contribution_approx( s.root, tree.pressures, nu, P ) *
                           density( ccn, P, tree.temperature );
                  } );
  std::discrete_distribution<int> d( weights.begin(), weights.end() );

  s = d( globalRng );
  C = concentrations[s];
  return d.probabilities()[s];
}


// Choose a transition by going deeper and deeper in the tree.
// Return (i,p), i being the transition index and p its probability.
std::pair<Transition, double> choose_transition_in_cluster( const Cluster& c,
                                                            const std::vector<double>& pressures,
                                                            double nu,
                                                            double P,
                                                            double C,
                                                            double T,
                                                            double proba,
                                                            double line_cutoff,
                                                            bool voigt_profile )
{
  if ( c.children.empty() )
  {
    // TODO tester le cas où une seule transition

    assert( !c.transitions.empty() );
    std::discrete_distribution<int> d(
        c.transitions.size(),
        0.,
        double( c.transitions.size() ),
        [&c, nu, P, C, T, line_cutoff, voigt_profile]( size_t i ) {
          return h_transition(
              c.transitions.data() + int( std::floor( i ) ), nu, P, C, T, line_cutoff, voigt_profile );
        } );

    int i = d( globalRng );

    return std::make_pair( c.transitions[i], proba * d.probabilities()[i] );
  }
  else
  {
    // Choose the child to sample
    std::discrete_distribution<int> d(
        c.children.size(), 0., double( c.children.size() ), [&c, &pressures, nu, P]( size_t i ) {
          // TODO : find a way to take concentration into account (interpolate ?)
          return contribution_approx( c.children[int( std::floor( i ) )], pressures, nu, P );
        } );
    int i = d( globalRng );
    // sample the choosen child
    return choose_transition_in_cluster( c.children[i],
                                         pressures,
                                         nu,
                                         P,
                                         C,
                                         T,
                                         proba * d.probabilities()[i],
                                         line_cutoff,
                                         voigt_profile );
  }
}


// First sample a specie then a trasition of this specie.
// Return the transition index and its proba
TransitionIdWithProba probatree_sample_transition( const SpectralProbabilityTrees& tree,
                                                   double nu,
                                                   double P,
                                                   const std::vector<double>& concentrations,
                                                   unsigned int& specie )
{
  // Sample the species according to their concentration and contribution
  double C;
  double s_proba = probatree_specie_sampler( tree, nu, P, concentrations, specie, C );

  // Sample the selected specie at the requested concentration
  std::pair<Transition, double> res;
  res = choose_transition_in_cluster( tree.species[specie].root,
                                      tree.pressures,
                                      nu,
                                      P,
                                      C,
                                      tree.temperature,
                                      1.,
                                      tree.species[specie].line_cutoff,
                                      tree.species[specie].voigt_profile );
  return { res.first.id, res.first.nu_0, s_proba * res.second };
}


TransitionIdWithProba transition_sampler_proba_exact( const SpectralProbabilityTrees& tree,
                                                      double nu,
                                                      double P,
                                                      const std::vector<double>& concentrations,
                                                      unsigned int& specie )
{
  // 1. Compute probablities for each transition
  size_t num_transitions_total = std::accumulate(
      tree.species.begin(), tree.species.end(), size_t( 0 ), []( double t, const Specie& s ) {
        return t + s.transitions.size();
      } );

  std::vector<double> weights;
  weights.reserve( num_transitions_total );

  double T = tree.temperature;
  for ( unsigned int s = 0; s < tree.species.size(); s++ )
  {
    const Specie& sp  = tree.species[s];
    double c  = concentrations[s];
    double lc = sp.line_cutoff;
    bool vp   = sp.voigt_profile;
    // we cannot call transition_probabilities because
    // here we have different species densities (not handled by this function)
    double dst = density( c, P, T );

    std::transform( sp.transitions.begin(),
                    sp.transitions.end(),
                    std::back_inserter( weights ),
                    [nu, P, c, T, lc, vp, dst]( const Transition& tr ) {
                      return dst*h_transition( &tr, nu, P, c, T, lc, vp );
                    } );
  }

  // 2. Sample a transition within the list, given the probas
  std::discrete_distribution<size_t> d( weights.begin(), weights.end() );
  size_t t  = d( globalRng );
  size_t id = t;
  for ( unsigned int s = 0; s < tree.species.size(); s++ )
  {
    if ( id < tree.species[s].transitions.size() )
    {
      specie = s;
      break;
    }
    id -= tree.species[s].transitions.size();
  }
  return { tree.species[specie].transitions[id].id,
           tree.species[specie].transitions[id].nu_0,
           d.probabilities()[t] };
}
