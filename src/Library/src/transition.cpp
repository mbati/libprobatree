/* Copyright (C) 2021-2024 Université Paul Sabatier, CNRS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */


#include <cmath>
#include "transition.h"
#include <lblu.h>

#include <iostream>
#include <fstream>
#include <sstream>
#include <stdexcept>
#include <filesystem>
#include <vector>
#include <numeric>
#include <algorithm>


/*
 *		Constants
 */

/*
 * See https://hitran.org/docs/definitions-and-units/
 * for all definitions and formulas implemented in this file
 * Formula numbers given in comments refer to this page
 */

//https://hitran.org/docs/definitions-and-units/
const PhysicalConstants physical_constants { .Na = 6.02214129e23,    // mol^{-1}
                                             .Kb = 1.380649e-23,     // m^2 kg s^{-2} K^{-1}
                                             // Kb is not in the units given in HITRAN, because this one is more classical 
                                             .c  = 299792458.0,      // m s^{-1}
                                             .R  = 8.205736608e-5 }; // m^3 atm mol^{-1} K^{-1}

const double CONST_GAMMA_D = std::sqrt( 2. * std::log( 2. ) * physical_constants.Kb * physical_constants.Na) / physical_constants.c;

const double SQRT_LOG_2 = std::sqrt( std::log( 2. ) );
const double SQRT_LOG_2_OVER_PI = std::sqrt( log( 2. ) / M_PI );
const double NA_OVER_R = 1e-6 * physical_constants.Na / physical_constants.R; 

// static double ref_pressure = 1.; // atm
static const double REF_T = 296.; // K


/***********************************************/

Transition init_transition(const int molec_id, const int iso_id, const double nu_0, 
                           const double intensity, const double gamma_air, 
                           const double gamma_self, const double E_low, const double n_air, 
                           const double shift, const int id)
{
  return Transition{molec_id, iso_id, get_molar_mass(molec_id, iso_id), spectro_get_Q(molec_id, iso_id, REF_T), nu_0, intensity, gamma_air, gamma_self, E_low, n_air, shift, id};
}
                            

// mapping between molecule id, isotopologue id and molar mass
static std::vector<std::vector<double>> molar_mass_list; // g
// mapping between molecule id and molecule name
static std::vector<std::string> molecule_name_list; // g

//TODO: change to asserts!
double get_molar_mass(int m, int iso)
{
  if (molar_mass_list.size() <= 0) { std::cerr<<"Should load molar mass file."<<std::endl; throw; }
  if (m > (int) molar_mass_list.size()) { throw std::invalid_argument( "too big value" ); }
  if (iso == 0) { iso = 10; } // special case for 10 that is indexed 0 in Hitran
  if (m <= 0 || iso <= 0) { throw std::invalid_argument( "molar mass and isotopologue should start at 1" ); }
  return molar_mass_list[m-1][iso-1]; // -1 because of Hitran's convention starting at 1 and vector at 0
}

// TODO: change to asserts!
std::string get_molecule_name(int m)
{
  if (m > (int) molecule_name_list.size()) { throw std::invalid_argument( "too big value" ); }
  return molecule_name_list[m-1]; // -1 because of Hitran's convention starting at 1 and vector at 0
}


/*
 *		Usefull functions to compute a unit transition absorption coefficient
 */

// Doppler half-width at half-maximum
double spectro_gamma_doppler(double nu_c, double temperature, double molar_mass)
{ 
  // Formula 5
  return nu_c * std::sqrt( temperature / molar_mass) * CONST_GAMMA_D;
}

// Lorentz half-width at half-maximum
double spectro_gamma_lorentz(const double gamma_air, const double gamma_self, const double n_air, 
                     const double pressure, const double partial_pressure, const double temperature)
{
  // Formula 6
  double gamma = partial_pressure*gamma_self + (pressure - partial_pressure)*gamma_air;
  return gamma * std::pow(REF_T / temperature, n_air);
}

// Lorentz half-width at half-maximum
double gamma_lorentz(const Transition* tr,
                     double pressure, double concentration, double temperature)
{
  // Formula 6
  return spectro_gamma_lorentz(tr->gamma_air, tr->gamma_self, tr->n_air, pressure, pressure*concentration, temperature);
}

double spectro_lorentz_profile(double gamma, double diff)
{
  // Formula 8
  return gamma / ( M_PI * ( gamma * gamma + diff * diff ) );
}

double spectro_voigt_profile(double wvnb, double nu_center, double molar_mass, 
                             double temperature, double gamma_L)
{
  double gamma_d = spectro_gamma_doppler(nu_center, temperature, molar_mass);
  double x = (wvnb - nu_center) / gamma_d * SQRT_LOG_2;
  double y =            gamma_L / gamma_d * SQRT_LOG_2;
  double k = sln_faddeeva(x, y);
  return k * SQRT_LOG_2_OVER_PI / gamma_d; // Formula 9
}

// Shifted central wavenumber due to pressure
double spectro_nu_center(const double nu_0, const double shift, const double P)
{
  // Formula 7
  return nu_0 + shift * P;
}

// Shifted central wavenumber due to pressure
double nu_center(const Transition* tr,
                 double P)
{
  // Formula 7
  return spectro_nu_center(tr->nu_0, tr->shift, P);
}


double spectro_get_Q(int molec_id, int iso_id, double temperature) 
{
  double gi = 0.; // Qu'est ce que c'est ?
  int iso = iso_id;
  double Q = 0.;
  
#pragma omp critical
{
  // BD_TIPS is not thread-safe !!
  if (iso == 0) iso = 10; // conventions
  BD_TIPS_2017(&molec_id, &temperature, &iso, &gi, &Q);
}
  return Q;
}


double spectro_intensity(const int molec_id,
                         const int iso_id,
                         const double intensity,
                         const double E_low,
                         double temperature,
                         double Q_T_ref,
                         double n_c)
{
  if (temperature == REF_T) return intensity;
  // Formula 4
  double Q = spectro_get_Q(molec_id, iso_id, temperature);
  const double C2 = 1.4387769;
  double fol = (1. - exp(-C2 * n_c / temperature)) / (1. - exp(-C2 * n_c / REF_T));
  return intensity * Q_T_ref/Q * exp(-C2 * E_low / temperature) / exp(-C2 * E_low / REF_T) * fol;
}

double intensity(const Transition* tr,
                 double temperature,
                 double n_c) 
{
  return spectro_intensity(tr->molec_id, tr->iso_id, tr->intensity, tr->E_low, temperature,tr->Q_T_ref,  n_c);               
}

double h_transition( const Transition* tr,
                     double nu,
                     double P,
                     double C,
                     double T,
                     double line_cutoff,
                     bool is_voigt_profile )
{
  double nu_c = nu_center(tr, P);
  // Ignore cut transitions
  if ( line_cutoff > 0. && std::abs( nu - nu_c ) > 1.0000001 * line_cutoff) return 0.;

  // Lorentzian half width at half maximum (formula 6)
  double gamma_l = gamma_lorentz(tr, P, C, T);
  double profile = 0.;

  if ( is_voigt_profile ) {
    profile = spectro_voigt_profile(nu, nu_c, tr->molar_mass, T, gamma_l); 
  } else {
    // Lorentz profile
    profile = spectro_lorentz_profile(gamma_l, nu - nu_c);  
  }

  double s = intensity(tr, T, nu_c);
  return s * profile * 100.; // * s * profile is in cm-1, we return in m^-1
}

// Compute molecular density
double spectro_density(const double partial_pressure, const double temperature)
{
  return NA_OVER_R * partial_pressure / temperature;
  // 1e-6 comes from a unit conversion
}

// Compute molecular density
double density( double C, double P, double T )
{
  return NA_OVER_R * C * P / T;
  // 1e-6 comes from a unit conversion
}

// Approximation of Voigt half-width-at-half-height
double approx_gamma_voigt(const Transition* tr,
                          double P,
                          double C,
                          double T)
{
  double gamma_l = gamma_lorentz(tr, P, C, T);
  double nu_c = nu_center(tr, P);
  double gamma_d = spectro_gamma_doppler(nu_c, T, tr->molar_mass);
  return sqrt(gamma_l*gamma_l + gamma_d*gamma_d);
}

/* ---------------------------------------------------
      Computations on list of transitions
   --------------------------------------------------- */

// Sum the h contribution of all transitions
double sum_of_h( const std::vector<Transition> &transitions,
                 double nu,
                 double P,
                 double C,
                 double T,
                 double line_cutoff,
                 bool voigt_profile )
{
  double v = 0;
  for ( const Transition& t : transitions )
      v+= h_transition( &t, nu, P, C, T, line_cutoff, voigt_profile );
  return v;
}

// Compute the absorption coefficient on several transitions
// The density is only computed once (because the same for all transitions)
double absorption_coefficient( const std::vector<Transition>& transitions,
                               double nu,
                               double P,
                               double C,
                               double T,
                               double line_cutoff,
                               bool voigt_profile )
{
  return density( C, P, T )
         * sum_of_h(transitions, nu, P, C, T, line_cutoff, voigt_profile);
}

// Return proba of each transition in the input list
void transition_probabilities( const std::vector<Transition>& transitions,
                               double nu,
                               double P,
                               double C,
                               double T,
                               double line_cutoff,
                               bool voigt_profile,
                               std::vector<std::pair<Transition, double>>& allWeights )
{
  if (transitions.size() == 0) { std::cerr<<"No transition"<<std::endl; throw; }

  std::vector<std::pair<Transition, double>> h_list;
  std::transform( transitions.begin(),
                  transitions.end(),
                  std::back_inserter( h_list ),
                  [nu, P, C, T, line_cutoff, voigt_profile]( const Transition& tr ) {
                      return std::make_pair(
                          tr, h_transition( &tr, nu, P, C, T, line_cutoff, voigt_profile ) );
                  } );

  double sum_of_h = std::accumulate(h_list.begin(), h_list.end(), 0.,
                                    []( double sum, const std::pair<Transition, double>& a ) { return sum + a.second; } );
  if (sum_of_h <= 0.) throw;

  std::transform( h_list.begin(),
                  h_list.end(),
                  std::back_inserter( allWeights ),
                  [sum_of_h]( const std::pair<Transition, double>& pair ) {
                     return std::make_pair(
                         pair.first, pair.second/sum_of_h );
                  } );
}


/* ---------------------------------------------------
   Load Hitran data (molar mass file and transitions)
   --------------------------------------------------- */

#define MOLAR_FILE_PATH M_FILE

// Splits a string into substring according to the given delimiter
void string_split( std::string& s,
                   const std::string& delimiter,
                   std::vector<std::string>& values )
{
  size_t pos;
  std::string token;
  while ( ( pos = s.find( delimiter ) ) != std::string::npos )
  {
    token = s.substr( 0, pos );
    values.push_back( token );
    s.erase( 0, pos + delimiter.length() );
  }
  values.push_back( s );
}

// Read and store the Hitran file containing molar masses
void load_molar_mass_file()
{
  #ifdef VERBOSE
    #ifdef ACTIVE_PARALLEL_COMPUTATION
    std::cout << "Exécution parallèle "<<std::endl;
    #else 
    std::cout << "Pas d'exécution parallèle "<<std::endl;
    #endif
  #endif 

    // If data already loaded : early exit
    if (molar_mass_list.size() > 0) return;

    std::string filename = MOLAR_FILE_PATH;

    std::ifstream file( filename );
    if ( file.is_open() )
    {
        std::string line;
        std::vector<double> molec_isotopes;
        getline( file, line ); // we skip the 1st line (no data)
        int iso, gj;
        double abundance, Q, molar_mass;
        std::string molec_name;
        int molec_id;
        while ( getline( file, line ) )
        {
            if ( line.empty() ) continue;
            std::stringstream linestream(line);

            if (! (linestream >> iso)) {
                // Molecule header
                std::vector<std::string> values;
                string_split(line, "(", values);
                molec_name = values[0];
                molec_id = std::stoi(values[1].substr(0, values[1].find(")")));
                if ((int) molecule_name_list.size() != molec_id -1) throw;
                // save index <--> molecule name
                molecule_name_list.push_back(molec_name);

                // save all previous isotopologue
                if (molec_isotopes.size() > 0) molar_mass_list.push_back(molec_isotopes);
                molec_isotopes.clear();
            } else {
                // Isotope data
                linestream >> abundance >> Q >> gj >> molar_mass;
                molec_isotopes.push_back(molar_mass*1e-3);
                // molar mass are provided in g and we store in kg due to our choice of k_B units
            }
        }
        if (molec_isotopes.size() > 0) molar_mass_list.push_back(molec_isotopes);
        file.close();
        // std::cout<< "Molar mass file loaded."<<std::endl;
    } else {
      std::cout << "Unable to open molar mass file " << filename << std::endl;
      throw;
    }
}

// Removes empty strings from the given vector of strings
std::vector<std::string> remove_empty_strings( const std::vector<std::string>& values )
{
  std::vector<std::string> line;
  for ( const std::string& s : values )
  {
    if ( s.empty() ) continue;
    line.push_back( s );
  }
  return line;
}

// Load a Hitran set of transitions
void load_transitions( const std::string& path,
                       std::vector<Transition>& transitions )
{
  std::ifstream f( path );

  if ( f.is_open() )
  {
    std::string line;
    int i = 0; // count;
    while ( getline( f, line ) )
    {
      if ( line.empty() ) continue;
      if ( line[0] == '#' ) continue;
      std::vector<std::string> values;
      string_split( line, " ", values );
      values = remove_empty_strings( values );
      if ( values.size() < 9 ) continue;
      Transition tr = init_transition( stoi( values[0] ),
                                       stoi( values[1] ),
                                       stod( values[2] ),
                                       stod( values[3] ),
                                       stod( values[4] ),
                                       stod( values[5] ),
                                       stod( values[6] ),
                                       stod( values[7] ),
                                       stod( values[8] ),
                                       i );
      transitions.push_back( tr );
      ++i;
    }
    f.close();
  }
  else
  {
    std::cerr << "Parse_csv : Cannot open " << path << std::endl;
    exit( 1 );
  }
}
