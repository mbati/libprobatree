/* Copyright (C) 2021-2024 Université Paul Sabatier, CNRS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */


#include "simplification.hpp"

#include <algorithm>

// Polyline simplification
// We use the Ramer–Douglas–Peucker method
// parametric : https://en.wikipedia.org/wiki/Ramer%E2%80%93Douglas%E2%80%93Peucker_algorithm
// non parametric https://www.sciencedirect.com/science/article/pii/S0262885612000984?via%3Dihub
//
// We propose our own version when a strict majorant is needed,
// by prohibiting the positive errors.
// This simple algorithm should be improved!
// Because no simplification is done on convex parts

/* ---------------------------------
                Distances
   --------------------------------- */

// Return the slope of the (p_start, p_end) line
double slope( const Point& p_start,
              const Point& p_end)
{
  return (p_end.contrib - p_start.contrib) / (p_end.nu - p_start.nu);
}


// Linear approximation of the signed distance
// between a point p and a segment of slope s and passing through the point refPoint.
//                   p
//                   |       q
//   r               |                  with (r, q) of slope s
double signed_linear_approx_distance( const Point& r,
                                      const Point& p,
                                      double s)
{
  return ( r.nu - p.nu )*s + p.contrib - r.contrib;
}


double distance( const Point& refPoint,
                 const Point& p,
                 double s)
{
  return signed_linear_approx_distance(refPoint, p, s);
}


/* ---------------------------------
             RDP algorithm
   --------------------------------- */

// Given a list of points, to be considered in the interval [istart, iend],
// compute and return the max (signed s and unsigned u) distance
// index (imax) and value (dmax)
// between the segment [points[istart], points[iend]]
// and each points[i] with istart < i < iend
void
find_max_error_point( int istart, int iend,
                      const std::vector<Point>& points,
                      int& s_imax, double& s_dmax,
                      int& u_imax, double& u_dmax)
{
  Point start  = points.at( istart );
  Point end    = points.at( iend );
  // Compute the segment [end, start] slope
  double d_h = slope(start, end);

  for ( int i = istart + 1; i < iend; i++ )
  {
    double error_lin = distance(start, points[i], d_h);

    // keep the max signed / unsigned distances and their index
    if ( error_lin > s_dmax ) { s_imax = i; s_dmax = error_lin; }
    if ( std::abs(error_lin) > u_dmax ) { u_imax = i; u_dmax = std::abs(error_lin); }
  }
}


// Custom Ramer-Douglas-Peucker line simplification algorithm
// with eps being the maximal relative error
// and majorant_RDP (de)activates the classical s custom RDP method
// Return the effective maximal error and the simplified polyline
std::vector<Point>
RDP_line_simplification( std::vector<Point> points,
                         double eps,
                         bool majorant_RDP,
                         double& error_max )
{
  std::vector<std::pair<int, int>> endpoint_indices;
  endpoint_indices.emplace_back( 0, points.size() - 1 );
  error_max = 0.;
  std::vector<Point> simplified_line;
  simplified_line.push_back( points.front() );
  simplified_line.push_back( points.back() );
  while ( !endpoint_indices.empty() )
  {
    auto [first_point, last_point] = endpoint_indices.back();
    endpoint_indices.pop_back();
    if ( last_point - first_point <= 1 ) continue;

    // Signed and unsigned distances (with vector indices)
    // from each point to the segment [ first_point, last_point ]
    int u_index = 0; double u_error = 0.;
    int s_index = 0; double s_error = 0.;
    find_max_error_point( first_point, last_point, points,
                          s_index, s_error,
                          u_index, u_error );

    int index = u_index;
    double error = u_error;
    bool split = false;

    // We need to split again in two cases, when :
    // - positive error (convexity) [ONLY if strict majorant required]
    // - absolute error is bigger than (relative) epsilon
    if (majorant_RDP && s_error > 0.)
    {
      split = true;
      index = s_index;
      error = s_error;
    }
    else
    {
      if (points[index].contrib > 0)  error /= points[index].contrib; // RELATIVE ERROR
      split = error > eps;
    }

    if ( split )
    {
      // Recurse on the two sub-segments
      endpoint_indices.emplace_back( first_point, index );
      endpoint_indices.emplace_back( index, last_point );
      // Save this point to the simplification
      simplified_line.push_back( points.at( index ) );
    }
    else
    {
      // All the points can be simplified by the extremities
      error_max = std::max( error_max, u_error );
    }
  }
  std::sort(simplified_line.begin(), simplified_line.end()); 
  return simplified_line;
}


/* ---------------------------------
             Main function
   --------------------------------- */

double simplify( std::vector<Point>& points,
                 double precision,
                 bool majorant )
{
  if (precision <= 0.) return 0.; // no simplification
  double errorMax  = 0.;
  points           = RDP_line_simplification( points, precision, majorant, errorMax );
  return errorMax;
}
