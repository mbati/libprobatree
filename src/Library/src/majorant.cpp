/* Copyright (C) 2021-2024 Université Paul Sabatier, CNRS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */


#include "majorant.hpp"
#include "simplification.hpp"
#include <vector>
#include <algorithm>

#ifdef HAS_PARALLEL_POLICY
#  include <execution>
#endif


// sort and remove duplicates from the list
void remove_duplicates( std::vector<double>& points ) {
  std::sort( points.begin(), points.end() );
  auto last_point = std::unique( points.begin(), points.end() );
  points.erase( last_point, points.end() );
}

#ifdef TRACE_SIMPLIFICATION
#include <iostream>
#endif

// TODO the function below should be updated, not enough points when
// considering more than one transition per leaf

// Discretize wavenumbers depending on a transition (under thermo conditions),
// ADDED to xs vector.
// EMPIRICAL rule : we put more steps where the transition has a high value.
void discretize_wavenumbers_transition(const Transition* transition,
                                       double pressure,
                                       double concentration,
                                       double temperature,
                                       double line_cutoff,
                                       bool voigt_profile,
                                       std::vector<double>& xs)
{
  (void) concentration; (void) temperature;
  (void) line_cutoff; (void) voigt_profile;

  double nu = nu_center(transition, pressure); // TODO computed twice...
  // double ga = gamma_lorentz(transition, pressure, concentration, temperature) / 4.;
  // // Add point between the last one and the current transition's central wavenumber
  // if ( !xs.empty() ) { xs.push_back( .5 * ( xs.back() + nu ) ); }
  xs.push_back( nu );


  // The following line is arbitrary
/*  std::vector<double> prefactor = { 1.0, 4.0, 8.0 };
  for (int i = 0; i < (int) prefactor.size(); i++) {
    if (prefactor[i] * ga > line_cutoff) {
      xs.push_back( nu - line_cutoff );
      xs.push_back( nu + line_cutoff );
      return;
    }
    xs.push_back( nu - prefactor[i] * ga );
    xs.push_back( nu + prefactor[i] * ga );
  }*/
}


// Discretize wavenumbers between a transition central wavenumber (under thermo conditions) and a given boundary.
// This discretization is not uniform : it depends on the transition's width,
// it does not contain the transition central wavenumber but the boundary.
// These new wavenumbers are ADDED to xs.
void discretize_wavenumbers_interval(const Transition* transition,
                                     double boundary,
                                     double pressure,
                                     double concentration,
                                     double temperature,
                                     bool voigt_profile,
                                     std::vector<double>& xs)
{
  double nu = nu_center(transition, pressure);
  double ga = voigt_profile? approx_gamma_voigt(transition, pressure, concentration, temperature) : gamma_lorentz( transition, pressure, concentration, temperature );
  // double ga = gamma_lorentz( transition, pressure, concentration, temperature );
  ga /= 4.;
  if (nu<boundary) {
    // Increase
    for (double s = 1.; nu + s * ga < boundary; s*=2 )
      xs.push_back( nu + s * ga );
  } else {
    // Decrease
    for (double s = -1.; nu + s * ga > boundary; s*=2 )
      xs.push_back( nu + s * ga );
  }
  xs.push_back(boundary);
}


void polyline_majorant(const std::vector<Point>& points,
                       std::vector<Point>& majorant,
                       bool strict_majorant)
{
  majorant = points; // x values of each point is copied, modify y values
  if (! strict_majorant) return;

  for ( unsigned int i = 1; i < points.size()-1; i++ )
  {
    // Apply majoration by taking the max in neighborhood
    majorant[i].contrib = std::max(points[i].contrib,std::max(points[i-1].contrib, points[i+1].contrib));
  }
}


void build_leaf_majorant(const std::vector<Transition> &transitions,
                         double pressure,
                         double concentration,
                         double temperature,
                         double line_cutoff,
                         bool voigt_profile,
                         double precision,
                         bool majorant,
                         std::vector<Point> &points,
                         double &trunc_left,
                         double &trunc_right)
{
  // 1. We select the wavenumbers at which the polyline will be evaluated
  std::vector<double> xs;
  xs.reserve( 4 * transitions.size() );

  trunc_left  = nu_center(&transitions[0], pressure);
  trunc_right = nu_center(&transitions.back(), pressure);

  // Add samples where the transitions are distributed
  for ( Transition tr : transitions )
  {
    double nu = nu_center(&tr, pressure);

    // This must be done as transitions order can change according to shift and pressures
    // Will need a reduction operator for openmp version of this loop.
    trunc_left  = std::min( trunc_left, nu );
    trunc_right = std::max( trunc_right, nu );

    xs.push_back(nu);
    // TODO: should have several point per individual line when considering several lines
    //discretize_wavenumbers_transition(&tr, pressure, concentration, temperature, line_cutoff, voigt_profile, xs);
  }
  trunc_left  = std::max( 0., trunc_left - line_cutoff );
  trunc_right += line_cutoff;

  // Add samples just before / after the first / last transition
  // TODO: first and last transitions could be changed according to pressure!
  discretize_wavenumbers_interval(&transitions.front(), trunc_left, pressure, concentration, temperature, voigt_profile, xs);
  discretize_wavenumbers_interval(&transitions.back(), trunc_right, pressure, concentration, temperature, voigt_profile, xs);
  remove_duplicates( xs );

  // 2. Evaluate the exact sum of transitions for the chosen wavenumbers
  std::vector<Point> temp_points( xs.size() );
  for ( unsigned int i = 0; i < xs.size(); i++ ) {
    temp_points[i].nu = xs[i]; 
    temp_points[i].contrib = sum_of_h(transitions, xs[i], pressure, concentration, temperature, line_cutoff, voigt_profile);
  }

  // 3. Build the majorant
  points.reserve(temp_points.size());
  polyline_majorant(temp_points, points, majorant);

  // 4. Apply simplification on the polyline (point decimation)
  simplify( points, precision, majorant );
}
