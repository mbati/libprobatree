/* Copyright (C) 2021-2024 Université Paul Sabatier, CNRS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */


#include "cluster.hpp"
#include <iostream>
#include <algorithm>

/// FORMAT
/// number of species
/// number of pressures
/// pressures: list of doubles

/// for each cluster:
/// number of children: unsigned int
/// if leaf:
///     number of transitions: unsigned int
///     transitions: list of doubles
/// trunc_left, trunc_right: doubles
/// pour toute pression P:
///     number of points: unsigned int
///     points: list of doubles
/// data of child 1
/// data of child 2
/// etc
/// No need for pressures vector, just give the number of pressures

void serialize_cluster( Cluster& c, std::fstream& f, std::vector<double>& pressures ) {
  unsigned int s = c.children.size();
  f.write( reinterpret_cast<char*>( &s ), sizeof( unsigned int ) );

  if ( c.children.empty() )
  {
    s = c.transitions.size();
    f.write( reinterpret_cast<char*>( &s ), sizeof( unsigned int ) );

    for ( Transition tr : c.transitions )
    {
      f.write( reinterpret_cast<char*>( &tr.molec_id ), sizeof( int ) );
      f.write( reinterpret_cast<char*>( &tr.iso_id ), sizeof( int ) );
      f.write( reinterpret_cast<char*>( &tr.molar_mass ), sizeof( double ) );
      f.write( reinterpret_cast<char*>( &tr.Q_T_ref ), sizeof( double ) );
      f.write( reinterpret_cast<char*>( &tr.nu_0 ), sizeof( double ) );
      f.write( reinterpret_cast<char*>( &tr.intensity ), sizeof( double ) );
      f.write( reinterpret_cast<char*>( &tr.gamma_air ), sizeof( double ) );
      f.write( reinterpret_cast<char*>( &tr.gamma_self ), sizeof( double ) );
      f.write( reinterpret_cast<char*>( &tr.E_low ), sizeof( double ) );
      f.write( reinterpret_cast<char*>( &tr.n_air ), sizeof( double ) );
      f.write( reinterpret_cast<char*>( &tr.shift ), sizeof( double ) );
      f.write( reinterpret_cast<char*>( &tr.id ), sizeof( int ) );
    }
  }

  for ( unsigned int iP = 0; iP < pressures.size(); iP++ )
  {
    f.write( reinterpret_cast<char*>( &c.trunc_left[iP] ), sizeof( double ) );
    f.write( reinterpret_cast<char*>( &c.trunc_right[iP] ), sizeof( double ) );
  }

  for ( unsigned int iP = 0; iP < pressures.size(); iP++ )
  {
    s = c.points[iP].size();
    f.write( reinterpret_cast<char*>( &s ), sizeof( unsigned int ) );

    for ( Point p : c.points[iP] )
    {
      f.write( reinterpret_cast<char*>( &p.nu ), sizeof( double ) );
      f.write( reinterpret_cast<char*>( &p.contrib ), sizeof( double ) );
    }
  }

  for ( Cluster& child : c.children )
  { serialize_cluster( child, f, pressures ); }
}

void probatree_serialize( SpectralProbabilityTrees& tree, std::fstream& f )
{
  double t = tree.temperature;
  f.write( reinterpret_cast<char*>( &t ), sizeof( double ) );

  unsigned int s = tree.pressures.size();
  f.write( reinterpret_cast<char*>( &s ), sizeof( unsigned int ) );

  for ( double P : tree.pressures )
  { f.write( reinterpret_cast<char*>( &P ), sizeof( double ) ); }

  // uncomment this when temperature ca be considered as a parameter
  //f.write( reinterpret_cast<char*>( &tree.temperature ), sizeof( double ) );

  unsigned int numspecie = tree.species.size();
  f.write( reinterpret_cast<char*>( &numspecie ), sizeof( unsigned int ) );

  for ( Specie& S : tree.species )
  {
    s = (unsigned int)(S.name.length());
    f.write( reinterpret_cast<char*>( &s ), sizeof( unsigned int ) );
    f.write( S.name.c_str(), s * sizeof( char ) );

    f.write( reinterpret_cast<char*>( &S.max_child_nb ), sizeof( int ) );
    f.write( reinterpret_cast<char*>( &S.line_cutoff ), sizeof( double ) );
    unsigned int b = S.voigt_profile;
    f.write( reinterpret_cast<char*>( &b ), sizeof( unsigned int ) );

    f.write( reinterpret_cast<char*>( &S.concentration ), sizeof( double ) );

    // TODO write the transitions ?
    // When tree will store only indices to concentrations,
    // implement the serialization of the transitions

    serialize_cluster( S.root, f, tree.pressures );
  }
}

/// No need for pressures vector, just give the number of pressures
Cluster unserialize_cluster( std::fstream& f,
                             std::vector<Transition>& transitions,
                             const std::vector<double>& pressures ) {
  unsigned int nchildren;
  f.read( reinterpret_cast<char*>( &nchildren ), sizeof( unsigned int ) );

  Cluster c;
  c.children.reserve( nchildren );

  if ( nchildren == 0 )
  {
    unsigned int ntransitions;
    f.read( reinterpret_cast<char*>( &ntransitions ), sizeof( unsigned int ) );
    c.transitions.reserve( ntransitions );

    for ( unsigned int i = 0; i < ntransitions; i++ )
    {
      Transition tr;
      f.read( reinterpret_cast<char*>( &tr.molec_id ), sizeof( int ) );
      f.read( reinterpret_cast<char*>( &tr.iso_id ), sizeof( int ) );
      f.read( reinterpret_cast<char*>( &tr.molar_mass ), sizeof( double ) );
      f.read( reinterpret_cast<char*>( &tr.Q_T_ref ), sizeof( double ) );
      f.read( reinterpret_cast<char*>( &tr.nu_0 ), sizeof( double ) );
      f.read( reinterpret_cast<char*>( &tr.intensity ), sizeof( double ) );
      f.read( reinterpret_cast<char*>( &tr.gamma_air ), sizeof( double ) );
      f.read( reinterpret_cast<char*>( &tr.gamma_self ), sizeof( double ) );
      f.read( reinterpret_cast<char*>( &tr.E_low ), sizeof( double ) );
      f.read( reinterpret_cast<char*>( &tr.n_air ), sizeof( double ) );
      f.read( reinterpret_cast<char*>( &tr.shift ), sizeof( double ) );
      f.read( reinterpret_cast<char*>( &tr.id ), sizeof( int ) );

      if (tr.molar_mass != get_molar_mass(tr.molec_id, tr.iso_id)) { throw "Error with molar mass value loaded"; }
      c.transitions.push_back( tr );
      transitions.push_back( tr );
    }
  }

  c.trunc_left.resize(pressures.size());
  c.trunc_right.resize(pressures.size());

  for ( unsigned int iP = 0; iP < pressures.size(); iP++ )
  {
    f.read( reinterpret_cast<char*>( &c.trunc_left[iP] ), sizeof( double ) );
    f.read( reinterpret_cast<char*>( &c.trunc_right[iP] ), sizeof( double ) );
  }

  for ( unsigned int iP = 0; iP < pressures.size(); iP++ )
  {
    unsigned int npoints;
    f.read( reinterpret_cast<char*>( &npoints ), sizeof( unsigned int ) );
    c.points.emplace_back();
    c.points[iP].reserve( npoints );
    for ( unsigned int i = 0; i < npoints; i++ )
    {
      Point p;
      f.read( reinterpret_cast<char*>( &p.nu ), sizeof( double ) );
      f.read( reinterpret_cast<char*>( &p.contrib ), sizeof( double ) );

      c.points[iP].push_back( p );
    }
  }

  for ( unsigned int i = 0; i < nchildren; i++ )
  {
    c.children.push_back( unserialize_cluster( f, transitions, pressures ) );
    c.children.back().parent = &c;
  }

  return c;
}

SpectralProbabilityTrees probatree_unserialize( std::fstream& f )
{
  SpectralProbabilityTrees tree;

  double temperature;
  f.read( reinterpret_cast<char*>( &temperature ), sizeof( double ) );
  tree.temperature = temperature;
  if (tree.temperature <= 0 || tree.temperature > 5000) throw;

  unsigned int npressures;
  f.read( reinterpret_cast<char*>( &npressures ), sizeof( unsigned int ) );
  if (npressures > 1000) throw;

  tree.pressures.resize(npressures);
  for ( unsigned int iP = 0; iP < npressures; iP++ )
  {
    double P;
    f.read( reinterpret_cast<char*>( &P ), sizeof( double ) );
    // tree.pressures.push_back( P );
    tree.pressures[iP] = P;
  }

  // uncomment this when temperature ca be considered as a parameter
  // f.read( reinterpret_cast<char*>( &tree.temperature ), sizeof( double ) );

  unsigned int numspecies;
  f.read( reinterpret_cast<char*>( &numspecies ), sizeof( unsigned int ) );
  tree.species.resize( numspecies );
  for ( unsigned int i = 0; i < numspecies; i++ )
  {
    unsigned int nl;
    f.read( reinterpret_cast<char*>( &nl ), sizeof( unsigned int ) );
    tree.species[i].name.resize( nl, '\0' );
    f.read( tree.species[i].name.data(), nl * sizeof( char ) );

    f.read( reinterpret_cast<char*>( &tree.species[i].max_child_nb ), sizeof( int ) );
    f.read( reinterpret_cast<char*>( &tree.species[i].line_cutoff ), sizeof( double ) );
    unsigned int voigt_profile;
    f.read( reinterpret_cast<char*>( &voigt_profile ), sizeof( unsigned int ) );
    tree.species[i].voigt_profile = voigt_profile;
    f.read( reinterpret_cast<char*>( &tree.species[i].concentration ), sizeof( double ) );

    // TODO read the transitions ?
    // When tree will store only indices to concentrations,
    // implement the unserialization of the transitions

    tree.species[i].root = unserialize_cluster( f, tree.species[i].transitions, tree.pressures );
    std::sort( tree.species[i].transitions.begin(),
               tree.species[i].transitions.end(),
               []( Transition a, Transition b ) { return a.nu_0 < b.nu_0; } );
  }

  probatree_sampler_seed( 0 );
  return tree;
}
