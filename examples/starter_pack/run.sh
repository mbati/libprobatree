#! /bin/bash -e

# Copyright (C) 2021-2024 Université Paul Sabatier, CNRS
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.


LANG=C

if [ "$#" -ne 1 ]; then
  echo "Usage : $0 path/to/build"
  exit
fi

build="$1/src/Tools/"
data=4_lines
tree=tree_"${data}"
dir=$( dirname $0 )

## Generate data
rm "${data}.out"
{
  echo "4" >> "${data}.out"
  echo "2 1 150 1.115e-30 0.069  5.0 3341.635 0.75 -4e-05"
  echo "2 1 153 1.137e-30 0.0692 2.5 3318.6487 0.74 -3.1e-05"
  echo "2 1 154 1.289e-30 0.0695 3.0 3297.4065 0.74 -4.4e-05"
  echo "2 1 160 1.308e-30 0.0698 10.0 3276.0359 0.73 -5.1e-05" 
} >> "${data}.out"

## Build tree
"${build}"/buildProbaTree/buildTree -b "${data}".out -n "${tree}" -s 1 -p 1 -c 0.2 -v -m
echo ""

## Extract number of transitions in data file
nb_tr=$( head -n 1 "${data}.out" )

## Generate the absorption coefficient figure
"${dir}"/../visu_scripts/print_tree.sh "${build}"/printProbaTreeData/printProbaTreeData "${tree}".sav "${nb_tr}"

## Generate the proba figure
"${build}"/printProbaTreeData/printProbaTreeData proba -tree "${tree}".sav -output proba_exact.txt -n 10000  -exact
"${build}"/printProbaTreeData/printProbaTreeData proba -tree "${tree}".sav -output proba_approx.txt -n 10000 -approx
python3 ${dir}/../visu_scripts/proba.py -a proba_approx.txt -e proba_exact.txt -f proba_plot

echo "You can watch proba_plot.png"

