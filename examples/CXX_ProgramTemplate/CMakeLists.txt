# Copyright (C) 2021-2024 Université Paul Sabatier, CNRS
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.


cmake_minimum_required(VERSION 3.12)
# configure the project
project( CXX_ProgramTempalte LANGUAGES CXX)
set(CMAKE_CXX_STANDARD 17)
set(CXX_STANDARD_REQUIRED True)

#-------------------------------------------------------------------------------
#  General settings -- Some part will be made automatically available by the
#       SpectralProbabilityTree package.
#-------------------------------------------------------------------------------
# Do not allow to build into the source dir
set(CMAKE_DISABLE_SOURCE_CHANGES ON)
set(CMAKE_DISABLE_IN_SOURCE_BUILD ON)
# set the default build-type to Release
if(NOT CMAKE_BUILD_TYPE)
    message(WARNING "No CMAKE_BUILD_TYPE, using default CMAKE_BUILD_TYPE=Release")
    set(CMAKE_BUILD_TYPE "Release")
endif()
# Set default install location to ${CMAKE_BINARY_DIR}/install/${CMAKE_BUILD_TYPE}
# we do not want to install to /usr by default
if (CMAKE_INSTALL_PREFIX_INITIALIZED_TO_DEFAULT)
    set(CMAKE_INSTALL_PREFIX "${CMAKE_SOURCE_DIR}/install/${CMAKE_BUILD_TYPE}" CACHE PATH
        "Install path prefix, prepended onto install directories." FORCE)
endif()
# Be nice to visual studio and others IDE
set_property(GLOBAL PROPERTY USE_FOLDERS ON)
# Set compile option for the project
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Wextra")

#-------------------------------------------------------------------------------
#  Project specific configuration
#-------------------------------------------------------------------------------
find_package(SpectralProbabilityTree REQUIRED)
# add more package requierements

# Configure the project
set(sources
    # add here the C++ source files
    )

set(headers
    # add here the C++ headers
    )

# configure the executable and link with the SpectralProbabilityTree library
add_executable(${PROJECT_NAME} ${sources} ${headers})
target_link_libraries(${PROJECT_NAME} PRIVATE MCGRad::SpectralProbabilityTree)
# add more target_link_libraries if needed

# install the program if required
install(
    TARGETS ${PROJECT_NAME}
    RUNTIME DESTINATION bin
)
