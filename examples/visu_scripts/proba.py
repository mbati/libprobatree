#!/usr/bin/python3

# Copyright (C) 2021-2024 Université Paul Sabatier, CNRS
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import sys, argparse, os
import tools

colored_exact = True

plt.rcParams['figure.dpi'] = 150


""" Returns command line arguments """
def parse_args(argv):
    parser = argparse.ArgumentParser(description='Draw probaTree data')
    parser.add_argument('--approx', '-a', nargs="+", help='Approximated data')
    parser.add_argument('--exact',  '-e', nargs="+", help='Exact data')
    parser.add_argument('--title',  '-t', nargs="?", help='Set title')
    parser.add_argument('--figname','-f', nargs="?", default='fig', help='Saved figure name')
    parser.add_argument('--show', action='store_true')
    return parser.parse_args()
#end def

def is_valid(fname):
    if not fname: return False
    if os.path.isfile(fname): return True
    print(fname, " is not a file.")
    sys.exit()
#end def
    
def plot_proba(axes, approx_file, exact_file):
    if is_valid(exact_file):
        xa, ya = tools.read_file_proba(exact_file)
        accum = np.array(ya[0]) 
        for i in range(len(xa)):
            axes[0].plot(xa[i], ya[i], '-', c="C"+str(i), label="line "+str(i))
            if i != 0: accum = accum + np.array(ya[i])
            axes[1].plot(xa[0], accum, c="C"+str(i))
        #end for
        # only for legend
        axes[0].plot([None], [None], '-', c="k", label="exact")
        axes[1].plot([None], [None], c="k", label="exact cumulative")
    #end if
    if is_valid(approx_file):
        xa, ya = tools.read_file_proba(approx_file)
        for i in range(len(xa)):
            axes[0].plot(xa[i], ya[i], '--', c="C"+str(i))
        #end for
        # only for legend
        axes[0].plot([None], [None], '--', c="k", label="approx.")
        
        axes[1].stackplot(xa[0], *[ya[i] for i in range(len(ya))], alpha=0.4)
    #end if
#end def 


def eval(v, i):
    return v[i] if i < len(v) else None


if __name__ == "__main__":
    args = parse_args(sys.argv)
    
    K = 2
    plot_tree = False
    if (args.approx is None): args.approx = [None]
    if (args.exact is None):   args.exact = [None]

    mpl.rcParams['legend.fontsize'] = "x-small"
    mpl.rcParams['legend.title_fontsize'] = 'small'
    mpl.rcParams['xtick.labelsize'] = 'x-small'
    mpl.rcParams['ytick.labelsize'] = 'x-small'
    mpl.rcParams['axes.labelsize'] = 'small'
    
    f, (ax1, ax2) = plt.subplots(1, 2, figsize=(9,4))
    axes = [ax1, ax2]

#    f = plt.figure(figsize=(9,9))
#    axes = [plt.gca()]
    plot_proba(axes, args.approx[0], args.exact[0])
    
    axes[0].set_ylabel("probability")
    axes[1].set_ylabel("cumulative probability")
    for ax in axes: 
        ax.legend()      
        ax.set_xlabel("$\\nu$")
    #end for                        
    if args.title:
        plt.suptitle(args.title) 
    #end if
    
    plt.tight_layout(pad=2.0, w_pad=2.0, h_pad=2.0)
 
    plt.savefig(args.figname+".pdf")
    plt.savefig(args.figname+".png")
    if args.show: plt.show()
#end if
