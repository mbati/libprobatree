#!/usr/bin/python3

# Copyright (C) 2021-2024 Université Paul Sabatier, CNRS
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.


import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import sys, argparse, os
import tools

colored_exact = False

plt.rcParams['figure.dpi'] = 300

""" Returns command line arguments """
def parse_args(argv):
    parser = argparse.ArgumentParser(description='Draw probaTree data')
    parser.add_argument('--approx', '-a', nargs="+", help='Approximated data')
    parser.add_argument('--exact',  '-e', nargs="+", help='Exact data')
    parser.add_argument('--unit',   '-u', nargs="+", help='Unit transitions')
    parser.add_argument('--title',  '-t', nargs="?", help='Set title')
    parser.add_argument('--figname','-f', nargs="?", default='fig', help='Saved figure name')
    parser.add_argument('--colored_exact', action='store_true')
    parser.add_argument('--show', action='store_true')
    return parser.parse_args()
#end def

def is_valid(fname):
    if not fname: return False
    if os.path.isfile(fname): return True
    print(fname, " is not a file.")
    sys.exit()
#end def

def plot_node(ax, row_idx, col_idx, approx_file, exact_file, unit_file):
    if is_valid(approx_file):
        is_valid(approx_file)
        xa, ya = tools.read_file_ka(approx_file)
        for i in range(len(xa)):
            #col = 'r' if not colored_exact or len(xa) == 1 else tools.get_color(row_idx,i)
            col = tools.get_color(row_idx+1,col_idx*2+i-1) if i != 0 else tools.get_color(row_idx, col_idx)
            ax.plot(xa[i], ya[i], '--', marker='o' if len(xa[i])<1000 else None, c=col, zorder=100-i, label="approx" if i == 0 else '', ms=3)
        #end for
    #end if
    
    if is_valid(exact_file):
        xe, ye = tools.read_file_ka(exact_file)
        for i in range(len(xe)): 
            #col = 'k' if not colored_exact or len(xe) == 1 else tools.get_color(row_idx,i)
            col = tools.get_color(row_idx+1,col_idx*2+i-1) if i != 0 else tools.get_color(row_idx, col_idx)
            ax.plot(xe[i], ye[i], '-', c=col, zorder=0, label="exact" if i == 0 else '')
        #end for
    #end if 

    if is_valid(unit_file):
        xu, yu = tools.read_file_ka_unit(unit_file)
        N_child = len(xu)
        for child in range(N_child):
            #color = "k" if N_child == 1 else tools.get_color(color_row_idx,child+1)
            color = tools.get_color(row_idx, col_idx) if N_child == 1 else tools.get_color(row_idx+1, 2*col_idx + child)
            for tr in range(len(xu[child])):
                #label="line "+str(child) if N_child>1 else "line "+str(tr)
                #if (N_child>1 and tr != 0): label=""
                ax.plot(xu[child][tr], yu[child][tr], ':', c=color, zorder=0) #, label=label)
            #end for
        #end for
        ax.plot([None], [None], ':', c="k", zorder=0, label="line")
    #end if 
#end def 


def eval(v, i):
    return v[i] if i < len(v) else None


if __name__ == "__main__":
    args = parse_args(sys.argv)
    colored_exact = args.colored_exact 
    
    K = 2
    plot_tree = False
    if (args.approx is None): args.approx = [None]
    if (args.exact  is None):  args.exact = [None]
    if (args.unit   is None):   args.unit = [None]
    
    if (len(args.approx) > 1 or len(args.exact) > 1 or len(args.unit) > 1):
        plot_tree = True;
    #end if
    
    mpl.rcParams['legend.fontsize'] = "x-small"
    mpl.rcParams['legend.title_fontsize'] = 'small'
    mpl.rcParams['xtick.labelsize'] = 'x-small'
    mpl.rcParams['ytick.labelsize'] = 'x-small'
    mpl.rcParams['axes.labelsize'] = 'small'
    
    fig = plt.figure(figsize=(9,9))
    axes = []
    
    if not plot_tree:
        axes.append(plt.gca())
        plot_node(axes[0], 0, 0, args.approx[0], args.exact[0], args.unit[0])
    else:
        # Create plot grid
        size = max(max(len(args.approx), len(args.exact)), len(args.unit))
        depth = int(np.ceil(np.log2(size + 1) -1))
        width = K**depth
        row_list = [0 for i in range(size)]; 
        col_list = [0 for i in range(size)];
        for i in range(size): 
            axes.append(plt.subplot2grid((depth+1, width), (row_list[i], col_list[i]*K**(depth-row_list[i])), 1, colspan=int(width/ K**row_list[i])))
            
            if col_list[i] == 0:
                #axes[i].autoscale(False)
                lpad = axes[i].yaxis.labelpad
                # axes[i].annotate("Depth "+str(row_list[i]), (0., 0.5), (-lpad-5.,0), axes[i].yaxis.label, rotation=90, size='large', ha='right', va='center', annotation_clip=False, textcoords="offset points")  
            #end if    
            if i == size-1: continue
            col_list[i+1] = col_list[i] + 1
            row_list[i+1] = row_list[i]
            if col_list[i+1] >= K**row_list[i+1]: row_list[i+1] += 1; col_list[i+1] = 0;
    	#end for
          
    	# Plot data in the grid
        for i in range(size):
            plot_node(axes[i], row_list[i], col_list[i], eval(args.approx,i), eval(args.exact,i), eval(args.unit,i))
        #end for
    	
    	# Add rectangle and preserve previous limits
        for i in range(size):
            x_lim,y_lim = axes[i].get_xlim(), axes[i].get_ylim()
            rect = plt.Rectangle((0,0), 1,1, edgecolor=tools.get_color(row_list[i], col_list[i]) if i != 0 else "w", fill=None, lw=3, zorder=-1, transform=axes[i].transAxes, clip_on=False)
            axes[i].add_patch(rect)
            axes[i].set_xlim(x_lim)
            axes[i].set_ylim(y_lim)
            if col_list[i] == 0: 
                axes[i].set_ylabel("$k_a$")
            #end if
    	#end for 
    #end if
    
    axes[0].legend()      
    for ax in axes: 
        #ax.set_ylabel("$k_a$")
        ax.set_xlabel("$\\nu$")
    #end for                        
    if args.title:
        plt.suptitle(args.title) 
    #end if
    
    #plt.tight_layout(pad=2.0, w_pad=2.0, h_pad=2.0)
    plt.tight_layout()
 
    plt.savefig(args.figname+".pdf")
    if args.show: plt.show()
#end if
