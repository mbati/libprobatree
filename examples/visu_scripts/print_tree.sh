#! /bin/bash -e

# Copyright (C) 2021-2024 Université Paul Sabatier, CNRS
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.


if [ "$#" -ne 3 ]; then
  echo "Usage $0 path/to/printProbaTreeData tree.sav nb_of_transitions"
  exit
fi

EXEC="$1"  # "src/Tools/printProbaTreeData/dump_majorant"
TREE="$2"  # "tree.sav" 
nb_tr="$3" # number of transitions in the tree
VISU_MAX_DEPTH=2 # max depth for visualization only
                 # 0 would mean that we print only the root

if (( VISU_MAX_DEPTH > 3 ))
then 
  echo "Stop, too many transitions, this visualization script might not be adapted."
fi

# Default parameters
temp=${TREE##*/}
DIR=${temp%.sav}_data
VISU=$( dirname "$0" )
STEPS=10000

# Computation on depth
DEPTH_f=$( echo "l(${nb_tr})/l(2)" | bc -l )
DEPTH=$( printf "%.0f" "${DEPTH_f}" )
full_tree=0 # true by default
if [ "${nb_tr}" -ne $(( 2**DEPTH )) ]; then 
  DEPTH=$(( DEPTH+1 ))
  full_tree=1 # false
fi
if [ "${nb_tr}" -gt $(( 2**VISU_MAX_DEPTH )) ]; then 
  full_tree=0 # true
fi

# Initialize list of files 
UNIT_FILES=""
APPROX_FILES=""


# ----------------- Functions 
#
# Internal function to compute the number of leaves at a given node
# numbered $2 at depth $1 in a tree composed of $3 transitions
#
compute_leaf_number_at_node () {
  # $1 depth, $2 child, $3 nb_transition
  if (( $1 == 0 )); then return; fi

  node_leaf_number=$3
  stock=0
  diff=$(( $1 -1 ))
  t=$(( 2**diff )) 
  right=10
  
  for d in $(seq 1 "$1")
  do
    right=$( printf "%.0f" $(( node_leaf_number/2 )) )
    diff=$(( $1 -d -1 ))
    if (( $2 < t ))
    then # left
      stock=$(( stock*2 ))
      node_leaf_number=$(( node_leaf_number - right ))
      if (( diff >= 0 )); then t=$(( t - 2**diff )); fi
    else # right
      stock=$(( stock*2 +1 ))
      node_leaf_number=${right}
      if (( diff >= 0 )); then t=$(( t + 2**diff )); fi
    fi
  done
}

# return 0 is node is a leaf
#
is_leaf () {
   # $1 depth, $2 child, $3 nb_transition
  if (( $1 == 0 )); then echo 0; return 0; fi

  compute_leaf_number_at_node "$1" "$2" "$3"
  if (( node_leaf_number == 1 && right == 1 )); then echo 1; return; fi
  echo 0; return; 
}

# return 0 if node is empty
#
is_empty () {
   # $1 depth, $2 child, $3 nb_transition
  if (( $1 == 0 )); then echo 1; return; fi

  compute_leaf_number_at_node "$1" "$2" "$3"
  if (( node_leaf_number == 1 && right == 1 )); then echo 1; return; fi
  if (( node_leaf_number > 1 )); then echo 1; return; fi
  echo 0; return;
}

# ----------------- End of functions



mkdir -p "${DIR}"
echo "Store all data in folder ${DIR}/"

# 1. Extract tree data and write it in file:
# - unit transitions
# - exact spectra
# - approximation or polyline (TODO: should change this variable name)

ACTUAL_DEPTH=$((VISU_MAX_DEPTH < DEPTH? VISU_MAX_DEPTH : DEPTH))
# for each level of the tree 
for d in $(seq 0 "${ACTUAL_DEPTH}")
do
    CHILDREN=$((2 ** d -1 )) # total node number for depth d
    
    # for each node at the current depth level d
    for c in $(seq 0 ${CHILDREN})
    do
        # echo "children ${c} / ${CHILDREN} for depth ${d}"
        
        res=$(is_empty "${d}" "${c}" "${nb_tr}")
        if [ "$res" -eq 0 ]; then continue; fi # ignore
 
        res=$(is_leaf "${d}" "${c}" "${nb_tr}")
        if [ "$res" -eq 1 ]
        then 
            # echo "leaf node" 
            # Write unit transition in file
            UNIT_NAME="${DIR}/data_d_${d}_c_${c}_unit_s_${STEPS}.txt"
            "${EXEC}" absorption -tree "${TREE}" -output "${UNIT_NAME}"  -node "${d}" "${c}" -n "${STEPS}" -transitions
            UNIT_FILES="${UNIT_FILES} ${UNIT_NAME}"
        
            # Exact transition spectrum
            EXACT_NAME="${DIR}/data_d_${d}_c_${c}_exact_s_${STEPS}.txt"
            "${EXEC}" absorption -tree "${TREE}" -output "${EXACT_NAME}" -node "${d}" "${c}" -n "${STEPS}" -exact
            EXACT_FILES="${EXACT_FILES} ${EXACT_NAME}"
            
            # Stored spectrum (polyline)
            APPROX_NAME="${DIR}/data_d_${d}_c_${c}_approx.txt"
            "${EXEC}" absorption -tree "${TREE}" -output "${APPROX_NAME}" -node "${d}" "${c}" -approx
            APPROX_FILES="${APPROX_FILES} ${APPROX_NAME}"
            
        else
            # echo "intermediate node" 
            # Evaluate current node, child 0 and 1 and concatenate the data
            d_child=$((d + 1))
            c_child0=$((2 * c))
            c_child1=$((c_child0 + 1))
            echo "#child -1" > header0
            echo "#child 0"  > header1
            echo "#child 1"  > header2
            
            # Unit transitions 
            UNIT_NAME="${DIR}/data_d_${d}_c_${c}_unit_s_${STEPS}.txt"
            "${EXEC}" absorption -tree "${TREE}" -output data1 -node "${d_child}" "${c_child0}" -n "${STEPS}" -transitions
            "${EXEC}" absorption -tree "${TREE}" -output data2 -node "${d_child}" "${c_child1}" -n "${STEPS}" -transitions
            cat header1 data1 header2 data2 > "${UNIT_NAME}"
            UNIT_FILES="${UNIT_FILES} ${UNIT_NAME}"
            
            # Exact spectra as sums of transitions 
            EXACT_NAME="${DIR}/data_d_${d}_c_${c}_exact_s_${STEPS}.txt"
            "${EXEC}" absorption -tree "${TREE}" -output data0 -node "${d}"       "${c}"        -n "${STEPS}" -exact
            "${EXEC}" absorption -tree "${TREE}" -output data1 -node "${d_child}" "${c_child0}" -n "${STEPS}" -exact
            "${EXEC}" absorption -tree "${TREE}" -output data2 -node "${d_child}" "${c_child1}" -n "${STEPS}" -exact
            cat header0 data0 header1 data1 header2 data2 > "${EXACT_NAME}"
            EXACT_FILES="${EXACT_FILES} ${EXACT_NAME}"
            
            # Stored spectra (polyline)
            APPROX_NAME="${DIR}/data_d_${d}_c_${c}_approx.txt"
            "${EXEC}" absorption -tree "${TREE}" -output data0 -node "${d}"       "${c}"        -approx
            "${EXEC}" absorption -tree "${TREE}" -output data1 -node "${d_child}" "${c_child0}" -approx
            "${EXEC}" absorption -tree "${TREE}" -output data2 -node "${d_child}" "${c_child1}" -approx
            cat header0 data0 header1 data1 header2 data2 > "${APPROX_NAME}"
            APPROX_FILES="${APPROX_FILES} ${APPROX_NAME}"
            
            rm -f header0 header1 header2 data0 data1 data2
        fi        
    done
done

# 2. Data visualization using these files
#
if (( full_tree == 0 ))
then
  echo """python3 ${VISU}/majorant.py -u ${UNIT_FILES} -e ${EXACT_FILES} -f ${DIR}/tree_construction_up -approx ${APPROX_FILES} --colored_exact """
  python3 "${VISU}"/majorant.py -u ${UNIT_FILES} -e ${EXACT_FILES} -f "${DIR}"/tree_visualization -a ${APPROX_FILES} --colored_exact 
  echo " "
  echo "You can open ${DIR}/tree_visualization.pdf"
else 
  echo " " 
  echo "Not able to visualize correctly the tree with Python:"
  echo "Require a full tree (a number of transitions equal to a power of 2)."
fi
exit;
