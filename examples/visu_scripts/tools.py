#!/usr/bin/python3

# Copyright (C) 2021-2024 Université Paul Sabatier, CNRS
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

import numpy as np

MY_COLORS = [["C0", "C1", "C5"],["C2","C4", "C6"]]
MY_COLORS = [["C5", "C7"], ["C0", "C1"],["C2","C4"]]
MY_COLORS = [["C2", "C4", "C3", "C7"], ["C0", "C1", "C5", "C6"]]

def get_color(row, col):
    if row == 0 : return "k"
    return MY_COLORS[row%2][col%4]
#end def


def add_data(x_add, y_add, x, y):
    x.append(np.array(list(map(float, x_add))))
    y.append(np.array(list(map(float, y_add))))
#end def


def read_file_proba(file):    
    x = []; x_temp = []
    y = []; y_temp = []
    with open(file,'r') as f:
        for line in f:
            if line[0:11] == "#transition": 
                # Add previous data
                if x_temp != []: 
                    add_data(x_temp, y_temp, x, y)
                #end if
                x_temp = []; y_temp = [];
                continue; 
            #end if
            temp = line.replace('\n', '').split('    ')
            x_temp.append( temp[0] )
            y_temp.append( temp[1] )
        #end for 
        # add last transitions to last child
        add_data(x_temp, y_temp, x, y)
    #end with
    return x, y
#end def


def read_file_ka(file):    
    x = []; x_temp = []
    y = []; y_temp = []
    with open(file,'r') as f:
        for line in f:
            if line[0:6] == "#child": 
                # Add previous data
                if x_temp != []: 
                    add_data(x_temp, y_temp, x, y)
                #end if
                x_temp = []; y_temp = [];
                continue; 
            #end if
            if line[0] == "!": 
                return x, y
            temp = line.replace('\n', '').split('    ')
            x_temp.append( temp[0] )
            y_temp.append( temp[1] )
        #end for 
        # add last transitions to last child
        add_data(x_temp, y_temp, x, y)
    #end with
    return x, y
#end def

def read_file_ka_unit(file):
    x = []; x_data = []; x_temp = []
    y = []; y_data = []; y_temp = []
    with open(file,'r') as f:
        for line in f:
            if line[0:11] == "#transition":
                # Add previous transition to list
                if x_temp != []: 
                    add_data(x_temp, y_temp, x_data, y_data)
                #end if 
                x_temp = []; y_temp = [];
                continue;
            #end if
            if line[0:6] == "#child": 
                # Add previous transition to list
                if x_temp != []: 
                    add_data(x_temp, y_temp, x_data, y_data)
                #end if 
                x_temp = []; y_temp = [];
                # Add data to previous child 
                if x_data != []:
                    x.append(np.array(x_data))
                    y.append(np.array(y_data)) 
                #end if 
                x_data = []; y_data = [];
                continue; 
            #end if
            temp = line.replace('\n', '').split('    ')
            x_temp.append( temp[0] )
            y_temp.append( temp[1] )
        #end for 
        # add last transitions to last child
        add_data(x_temp, y_temp, x_data, y_data)
        x.append(np.array(x_data))
        y.append(np.array(y_data)) 
    #end with
    return np.array(x), np.array(y)
#end def

#def read_file(file):
#    with open(file,'r') as f: 
#        xy = [ line.replace('\n', '').split('    ') for line in f ]
#    #end with 
#    x, y = zip(*xy)
#    return np.array(list(map(float, x))), np.array(list(map(float, y)))
##end def
