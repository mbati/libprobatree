/* Copyright (C) 2021-2024 Université Paul Sabatier, CNRS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */


#include "cluster.hpp"
#include <iostream>

// Constants for all trees tested
int k_means_k = 2, leaf_size = 1;
int n_samples = 1000000;
bool is_voigt_profile = true;
double range_min = 100.;
double range_max = 106.;
int line_cutoff = 25.;

// Variables that will be tested
std::vector<double> pressures = { 0.01, 0.9 };
std::vector<double> concentrations = { 0.01, 0.9 };
std::vector<double> temperatures = { 296., 500. };

bool is_k_times_proba_bigger_than
    (std::vector<Transition>& transitions,
     const std::vector<double>& wavenumbers,
     double pressure, double concentration, double temperature,
     double epsilon = 0.05,
     bool majorant=true)
{
  std::vector<TransitionIdWithProba> trans_probas(n_samples);
  std::vector<double> h_voigt(n_samples);
  std::vector<double> ka_approx(n_samples);

  // Build the tree approximation
  SpectralProbabilityTrees tree;
  tree.temperature = temperature;
  tree.pressures = std::vector<double>(1, pressure);
  tree.species.push_back(classify_transitions( std::move( transitions ), k_means_k, leaf_size ));
  probatree_compile(tree, 0, [](int){}, line_cutoff, concentration, is_voigt_profile, epsilon, majorant );
  std::cout<<"Tree created, p "<<pressure<<" c "<<concentration<<" t "<<temperature<<std::endl;

  std::vector<double> con_vec = { concentration };

  // Compute the coefficients
  #pragma omp parallel for schedule(static, 4)
  for (int i = 0; i < n_samples; i++)
  {
    // P_J(j)
    // trans_probas[i] = specie_transition_sampler( tree, 0, wavenumbers[i], pressure, concentration);
    unsigned int specie_sampled;
    trans_probas[i] = probatree_sample_transition( tree, wavenumbers[i], pressure, con_vec, specie_sampled );
    if (specie_sampled != 0) {
      std::cerr<<"Should sample 0"<<std::endl; abort();
    }
    // Evaluate h_a for each transition
    h_voigt[i] = h_transition(&transitions[trans_probas[i].id], wavenumbers[i], pressure,
                              concentration, temperature, line_cutoff, is_voigt_profile);

    ka_approx[i] = probatree_khat(tree, wavenumbers[i], pressure, std::vector<double>(1, concentration));
  }

  // Perform the comparison for the given number of samples
  for (int i = 0; i < n_samples; i++)
  {
    double temp = h_voigt[i]/ (ka_approx[i] * trans_probas[i].proba);
    if ( temp < 0 || temp > 1 ) return false;
  }

  return true;
}

int main()
{
  load_molar_mass_file();

  probatree_sampler_seed(1);

  // Load transitions from file
  std::vector<Transition> transitions;
  load_transitions( DUMMY_DATA_FILE, transitions );
  std::cout<<transitions.size()<<" transitions loaded"<<std::endl;

  // Initialize random generator to replay if error
  std::mt19937 rng(0); rng.seed(2);
  // Generate the random wavenumbers once
  std::vector<double> wavenumbers(n_samples);
  std::uniform_real_distribution<double> dist( 0, 1 );
  for (int i = 0; i < n_samples; i++) wavenumbers[i] = range_min + dist(rng) * (range_max - range_min);

  // Launch the test!
  if (! is_k_times_proba_bigger_than(transitions, wavenumbers, 1.0,  1.0,  296.)) { std::cerr<<"Error detected"<<std::endl; throw; return EXIT_FAILURE; }
  if (! is_k_times_proba_bigger_than(transitions, wavenumbers, 0.01, 1.0,  296.)) { std::cerr<<"Error detected"<<std::endl; throw; return EXIT_FAILURE; }
  if (! is_k_times_proba_bigger_than(transitions, wavenumbers, 0.01, 0.01, 296.)) { std::cerr<<"Error detected"<<std::endl; throw; return EXIT_FAILURE; }
  if (! is_k_times_proba_bigger_than(transitions, wavenumbers, 0.01, 0.01, 500.)) { std::cerr<<"Error detected"<<std::endl; throw; return EXIT_FAILURE; }

  return 0;
}
