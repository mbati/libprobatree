/* Copyright (C) 2021-2024 Université Paul Sabatier, CNRS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */


#include "cluster.hpp"
#include <iostream>

#define DUMMY_FILE_PATH DUMMY_DATA_FILE

// Constants for all trees tested
int k_means_k = 2, leaf_size = 1;
int n_samples = 1000000;
bool is_voigt_profile = true;
double range_min = 100.;
double range_max = 106.;
int line_cutoff = 25.;

// Variables that will be tested
std::vector<double> pressures = { 0.01, 0.9 };
std::vector<double> concentrations = { 0.01, 0.9 };
std::vector<double> temperatures = { 296., 500. };


bool is_majorant(std::vector<Transition>& transitions,
                 const std::vector<double>& wavenumbers,
                 double pressure, double concentration, double temperature,
                 double epsilon = 0.05,
                 bool majorant=true)
{
  std::vector<double> ka_exact(n_samples);
  std::vector<double> ka_approx(n_samples);

  // Build the tree approximation
  SpectralProbabilityTrees tree;
  tree.temperature = temperature;
  tree.pressures = std::vector<double>(1, pressure);
  tree.species.push_back(classify_transitions( std::move( transitions ), k_means_k, leaf_size ));
  probatree_compile(tree, 0, [](int){}, line_cutoff, concentration, is_voigt_profile, epsilon, majorant );
  std::cout<<"Tree created, p "<<pressure<<" c "<<concentration<<" t "<<temperature<<std::endl;

  double dst = density( concentration, pressure, temperature );

  // Compute the approximation and exact sum coefficients
  #pragma omp parallel for
  for (int i = 0; i < n_samples; i++)
  {
    ka_exact[i]  = dst * sum_of_h (transitions, wavenumbers[i], pressure, concentration, temperature, line_cutoff, is_voigt_profile);
    ka_approx[i] = probatree_khat (tree, wavenumbers[i], pressure, std::vector<double>(1, concentration));
  }

  // Perform the comparison for the given number of samples
  for (int i = 0; i < n_samples; i++)
  {
    // Make sure majorant
    if (ka_approx[i] < 0. || ka_exact[i] < 0.) return false;
    if (ka_exact[i] > ka_approx[i])            return false;
  }

  return true;
}


int main()
{
  load_molar_mass_file();

  // Load transitions from file
  std::vector<Transition> transitions;
  load_transitions( DUMMY_DATA_FILE, transitions );
  std::cout<<transitions.size()<<" transitions loaded"<<std::endl;

  // Initialize random generator to replay if error
  std::mt19937 rng(0); rng.seed(2);
  // Generate the random wavenumbers once
  std::vector<double> wavenumbers(n_samples);
  std::uniform_real_distribution<double> dist( 0, 1 );
  for (int i = 0; i < n_samples; i++) wavenumbers[i] = range_min + dist(rng) * (range_max - range_min);

  // Launch the test!
  if (! is_majorant(transitions, wavenumbers, 1.0,  1.0,  296.)) { throw; return EXIT_FAILURE; }
  if (! is_majorant(transitions, wavenumbers, 0.01, 1.0,  296.)) { throw; return EXIT_FAILURE; }
  if (! is_majorant(transitions, wavenumbers, 0.01, 0.01, 296.)) { throw; return EXIT_FAILURE; }
  if (! is_majorant(transitions, wavenumbers, 0.01, 0.01, 500.)) { throw; return EXIT_FAILURE; }
  if (  is_majorant(transitions, wavenumbers, 1.0,  1.0,  296., 0.2, false)) { throw; return EXIT_FAILURE; }

  return 0;
}
