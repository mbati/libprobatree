/* Copyright (C) 2021-2024 Université Paul Sabatier, CNRS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */


#include "transition.h"
#include "cluster.hpp"
#include <iostream>

int main()
{
  /* Test the correctness of the molarmass file loading */
  load_molar_mass_file();

  std::vector<double> m = { get_molar_mass(1,  1),
                            get_molar_mass(55, 1),
                            get_molar_mass(2, 0) }; // CO2 -- isotope 10
  std::vector<double> res = { 18.010565*1e-3, 70.998286*1e-3, 49.001675*1e-3 };
  for (int i = 0; i < (int) res.size(); i++) {
    if (m[i] != res[i]) {
      std::cerr<<"Found "<<m[i]<<" instead of "<<res[i]<<std::endl;
      throw;
    }
  }


  /* Test the correctness of the h_transition function, given a line_cutoff */

  double nu_0 = 5.;
  double shift = 1.;
  bool voigt_profile = true;

  // Depending on the line cutoff and chosen pressure,
  // The result should be set to zero or not
  double temperature = 296.;
  double concentration = 0.1;

  double line_cutoff = 5.;
  std::vector<double> pressures = { 0.001, 1.0 };
  std::vector<double> nus = { 0., 1., 5.5, 6.0, 6.2, 10.0, 11.0, 15.0 };
  std::vector<std::vector<bool>> is_zero = {{false, false, false, false, false, false, true,  true},
                                            { true, false, false, false, false, false, false, true}};

  std::cout<<"Start test"<<std::endl;
  for (int p = 0; p < (int) pressures.size(); p++) {
    for (int w = 0; w < (int) nus.size(); w++) {
      Transition tr = init_transition(1, 1, nu_0, 10., 1., 1., 1., 1., p==0? 0. : shift, 0);
      double h_v = h_transition( &tr, nus[w], pressures[p],
                                 concentration, temperature,
                                 line_cutoff, voigt_profile );
      if (is_zero[p][w] && h_v != 0.) { throw; return EXIT_FAILURE; }
    }
  }


  /* Test the correctness of the tree approimation, given a line_cutoff */
  std::vector<double> concentrations = {concentration};
  for (int p = 0; p < (int) pressures.size(); p++) {
    for (int w = 0; w < (int) nus.size(); w++) {
      Transition tr = init_transition(1, 1, nu_0, 10., 10., 10., 1., 1., p==0? 0. : shift, 0);
      std::vector<Transition> transitions = { tr };

      SpectralProbabilityTrees tree;
      tree.temperature = temperature;
      tree.pressures = pressures;
      tree.species.push_back(classify_transitions( std::move( transitions ), 2, 1 ));
      probatree_compile(tree, 0, [](int){}, line_cutoff, concentration, voigt_profile, 0.05, true );

      double res = probatree_khat(tree, nus[w], pressures[p], concentrations);
      if (is_zero[p][w] && res != 0.) { throw; return EXIT_FAILURE; }
    }
  }


  std::cout<<" ------------------ "<<std::endl;

  line_cutoff = 0.05;
  nu_0 = 10.;
  Transition tr = init_transition(1, 1, nu_0, 10., 10., 10., 1., 1., 0., 0);
  std::vector<Transition> transitions = { tr };

  SpectralProbabilityTrees tree;
  tree.temperature = temperature;
  tree.pressures = pressures;
  tree.species.push_back(classify_transitions( std::move( transitions ), 2, 1 ));
  probatree_compile(tree, 0, [](int){}, line_cutoff, concentration, voigt_profile, 0.05, true );

  std::cout<<" Points approximating the absorption coefficient "<<std::endl;
  std::vector<Point> points = tree.species[0].root.points[0];
  for (int i = 0; i < (int) points.size(); i++)
  {
    Point my_point = points[i];
    std::cout<<my_point.nu<<" "<<my_point.contrib<<std::endl;
    if (my_point.nu < nu_0 - line_cutoff) { throw; return EXIT_FAILURE; }
    if (my_point.nu > nu_0 + line_cutoff) { throw; return EXIT_FAILURE; }
    if (my_point.contrib <= 0.) { throw; return EXIT_FAILURE; }
  }

  std::cout<<"Test passed"<<std::endl;
  return 0;
}
