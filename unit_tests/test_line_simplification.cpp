/* Copyright (C) 2021-2024 Université Paul Sabatier, CNRS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */


#include "simplification.hpp"
#include <iostream>

int error() {
  throw;
  return EXIT_FAILURE;
}

bool same_list(std::vector<Point> pts1, std::vector<Point> pts2)
{
  if (pts1.size() != pts2.size()) return false;
  for (int i = 0; i < (int) pts1.size(); i++) {
    if (pts1[i].nu != pts2[i].nu) return false;
    if (pts1[i].contrib != pts2[i].contrib) return false;
  }
  return true;
}

void print_list(std::vector<Point> pts1)
{
  for (int i = 0; i < (int) pts1.size(); i++)
    std::cout<<pts1[i].nu<<" "<<pts1[i].contrib<<std::endl;
}

int main()
{
  /* Test the distance computation */

  //  y =
  //    1                   p1
  //                        | <---- distance
  //    0    p0 ----------------------------- p2
  // x =     0              1                 2
  std::vector<Point> points = { Point(0,0),
                                Point(1,1),
                                Point(2,0) };

  double slp = slope(points[0], points[2]);
  if (signed_linear_approx_distance(points[0], points[1], slp) != 1.)
    return error();
  if (signed_linear_approx_distance(points[1], points[1], slp) != 0.)
    return error();


  //     .    .    .    p1   .
  //     .    .    .    |\   .
  //     .    .    .    | \  p2
  //     .    .    .    .    .
  //     .    .    .    .    .
  //     .    .    .    .    .
  //    p0    .    .    .    .
  std::vector<Point> pts = { Point(1,1),
                             Point(4,4),
                             Point(5,3) };

  double sl = slope(pts[0], pts[2]);
  if (signed_linear_approx_distance(pts[0], pts[1], sl) != 1.5)
    return error();
  if (signed_linear_approx_distance(pts[0], Point(pts[1].nu, 1.0), sl) != -1.5)
    return error();

  /* Test the line simplification */
  double _;
  std::vector<Point> aligned, simple, res;

  /*                 p2
               p1
         p0
  */
  aligned = { Point(1,0),
              Point(2,1.5),
              Point(3,3) };
  simple = { aligned[0], aligned[2] };

  res = RDP_line_simplification(aligned, 0.00001, true,  _);
  if (! same_list(res, simple)) return error();
  res = RDP_line_simplification(aligned, 0.00001, false, _);
  if (! same_list(res, simple)) return error();


  /*
         p0 ---- p1 ---------- p2
  */
  aligned = { Point(1,2.3),
              Point(2,2.3),
              Point(8,2.3) };
  simple = { aligned[0], aligned[2] };

  res = RDP_line_simplification(aligned, 0.00001, true,  _);
  if (! same_list(res, simple)) return error();
  res = RDP_line_simplification(aligned, 0.00001, false, _);
  if (! same_list(res, simple)) return error();


  //     .    .    .    p1   .
  //     .    .    .    .    .
  //     .    .    .    .    p2
  //     .    .    .    .    .
  //     .    .    .    .    .
  //     .    .    .    .    .
  //    p0    .    .    .    .
  std::vector<Point> my_points;
  my_points = { Point(1,1),
                Point(4,4),
                Point(5,3) };
  simple = { my_points[0], my_points[2] };

  // SMALL EPSILON: no simplification
  res = RDP_line_simplification(my_points, 0.00001, true,  _);
  if (! same_list(res, my_points)) return error();
  res = RDP_line_simplification(my_points, 0.00001, false, _);
  if (! same_list(res, my_points)) return error();

  // HIGH EPSILON: simplification if no majorant option only
  // or majorant and small negative epsilon
  res = RDP_line_simplification(my_points, 10.0, true,  _);
  if (! same_list(res, my_points)) return error();
  res = RDP_line_simplification(my_points, 10.0, false, _);
  if (! same_list(res, simple)) return error();
  my_points[1].contrib = 2.;
  res = RDP_line_simplification(my_points, 10.0, true, _);
  if (! same_list(res, simple)) return error();

  std::cout<<"Test passed"<<std::endl;
  return 0;
}
