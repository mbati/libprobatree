/* Copyright (C) 2021-2024 Université Paul Sabatier, CNRS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */


#include "cluster.hpp"
#include "transition.h"
#include <iostream>

int main()
{
  std::vector<Point> points = {{0,1}, {1,2}, {2,1}, {4,3}};

  std::vector<std::pair<double, double>> expected = {{-0.5, 0. },
                                                     { 0. , 1. },
                                                     { 0.5, 1.5 },
                                                     { 2. , 1. },
                                                     { 3. , 2. },
                                                     { 4. , 3. },
                                                     { 5. , 0. }};

  for (int i = 0; i < (int) expected.size(); i++)
  {
    double temp = lerp( points, expected[i].first );
    std::cout<<expected[i].first<<" : "<<temp<<std::endl;
    if (temp != expected[i].second) { throw; return EXIT_FAILURE; }
  }

  std::cout<<"Test passed"<<std::endl;
  return 0;
}
