/* Copyright (C) 2021-2024 Université Paul Sabatier, CNRS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */


#include "cluster.hpp"
#include <iostream>

// TODO: Test for one specie only, should be several


int main()
{
  load_molar_mass_file();

  // Tree parameters
  int k_means_k = 2, leaf_size = 1;
  bool voigt_profile = true;
  int line_cutoff = 25.;
  std::vector<double> pressures = { 0.01, 0.9 };
  double concentration = 0.01;
  double temperature = 300.;
  int n_samples = 1000;
  double range_min = 100.;
  double range_max = 106.;
  double epsilon = 0.05;
  bool majorant=true;
  std::string temp_tree = "test_tree.sav";

  // Load transitions from file
  std::vector<Transition> transitions;
  load_transitions( DUMMY_DATA_FILE, transitions );
  std::cout<<transitions.size()<<" transitions loaded"<<std::endl;

  // Build the tree approximation
  SpectralProbabilityTrees tree1, tree2;
  tree1.temperature = temperature;
  tree1.pressures = pressures;
  tree1.species.push_back(classify_transitions( std::move( transitions ), k_means_k, leaf_size ));
  probatree_compile(tree1, 0, [](int){}, line_cutoff, concentration, voigt_profile, epsilon, majorant );

  // Save tree to disk
  std::fstream f;
  f.open(temp_tree, std::ios::out | std::ios::binary);
  probatree_serialize( tree1, f );
  f.close();

  // Reload the tree
  std::fstream in;
  in.open(temp_tree, std::ios::in | std::ios::binary);
  if ( in.fail() ) return EXIT_FAILURE;
  tree2 = probatree_unserialize( in );

  // Initialize random generator to replay if error
  std::mt19937 rng(0); rng.seed(2);
  // Generate the random wavenumbers once
  std::vector<double> wavenumbers(n_samples);
  std::uniform_real_distribution<double> dist( 0, 1 );
  for (int i = 0; i < n_samples; i++) wavenumbers[i] = range_min + dist(rng) * (range_max - range_min);

  std::cout<<"Launch the test"<<std::endl;

  // Test if the tree contents are equal
  for (int i = 0; i < n_samples; i++)
  {
    for (int j = 0; j < (int) pressures.size(); j++)
    {
/*    // Cannot test trasition sampling because of randomness
      TransitionIdWithProba proba_transition1 = specie_transition_sampler(tree1, 0, wavenumbers[i], pressures[j], concentration);
      TransitionIdWithProba proba_transition2 = specie_transition_sampler(tree2, 0, wavenumbers[i], pressures[j], concentration);
      if (proba_transition1.id    != proba_transition2.id)    { std::cout<<proba_transition1.id<<" "<<proba_transition2.id<<std::endl; throw; return EXIT_FAILURE; }
      if (proba_transition1.nu_c  != proba_transition2.nu_c)  { std::cout<<proba_transition1.nu_c<<" "<<proba_transition2.nu_c<<std::endl; throw; return EXIT_FAILURE; }
      if (proba_transition1.proba != proba_transition2.proba) { std::cout<<proba_transition1.proba<<" "<<proba_transition2.proba<<std::endl; throw; return EXIT_FAILURE; }
*/
      double ka_approx1 = probatree_khat(tree1, wavenumbers[i], pressures[j], std::vector<double>(1, concentration));
      double ka_approx2 = probatree_khat(tree2, wavenumbers[i], pressures[j], std::vector<double>(1, concentration));
      if (ka_approx1 != ka_approx2) { throw; return EXIT_FAILURE; }
    }
  }

  std::cout << tree1.species[0].root.trunc_left[0]<<" "<<tree2.species[0].root.trunc_left[0]<<std::endl; 
  if (tree1.species[0].root.trunc_left[0] != tree2.species[0].root.trunc_left[0]) { throw; return EXIT_FAILURE; }

  std::cout<<"Test passed"<<std::endl;
  return 0;
}
